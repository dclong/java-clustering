package common;

import java.util.ArrayList;


public class DistPartitionPrior implements PartitionPrior{
	
	private dclong.util.Point[] pts;

	private double coefficient;
	
	private FragmentPenalty fragmentPenalty;
	
	private SizePenalty sizePenalty;
	/**
	 * 
	 * @param locs
	 * @param coefficient
	 * @param fragmentPenalty
	 * @param sizePenalty
	 */
	public DistPartitionPrior(double[][] locs, double coefficient,
			FragmentPenalty fragmentPenalty,SizePenalty sizePenalty){
		pts = new dclong.util.Point[locs.length];
		for (int i = 0; i < locs.length; ++i) {
			pts[i] = new dclong.util.Point(locs[i][0], locs[i][1]);
		}
		this.coefficient = coefficient;
		//shallow copy of penalty objects
		this.fragmentPenalty = fragmentPenalty;
		this.sizePenalty = sizePenalty;
	}
	
	public double logPrior(ArrayList<ArrayList<Integer>> partition){
		return -coefficient*partitionComplexity(partition);
	}
	
	private double partitionComplexity(ArrayList<ArrayList<Integer>> partition) {
		// get number of subsets
		int size = partition.size();
		if (size <= 0) {
			return 0;
		}
		double complexity = 0;
		ArrayList<Integer> set;
		for (int i = 0; i < size; i++) {
			set = partition.get(i);
			complexity += classComplexity(set);
		}
		return complexity / size + sizePenalty.penalty(partition) + fragmentPenalty.penalty(partition);
	}
	
	private double classComplexity(ArrayList<Integer> set) {
		int size = set.size();
		if (size <= 1) {
			return 0;
		}
		double sumDist = 0;
		for(int i=size-1; i>=1; --i){
			dclong.util.Point currentPoint = pts[set.get(i)];
			for(int j=i-1; j>=0; --j){
				sumDist += currentPoint.distance(pts[set.get(j)]);
			}
		}
		return sumDist / (0.5 * size * (size - 1));
	}
	
	public double classComplexity(){
		ArrayList<Integer>	set = new ArrayList<Integer>(20);
		for(int i=0; i<20; ++i){
			set.add(i);
		}
		return classComplexity(set);
	}
}

package common;

import java.util.ArrayList;


public class EntropyPrior implements PartitionPrior{
	private double coefficient;
	
	public EntropyPrior(double coefficient){
		this.coefficient = coefficient;
	}
	@Override
	public double logPrior(ArrayList<ArrayList<Integer>> partition) {
		return Math.log(1-Math.exp(entropy(partition)*coefficient));
	}
	
	private double entropy(ArrayList<ArrayList<Integer>> partition){
		int size = partition.size();
		double e = 0;
		for(int i=0; i<size; ++i){
			int k = partition.get(i).size();
			e += k*Math.log(k);
		}
		return -e;
	}
}

package common;

import java.util.ArrayList;

public class FragmentPartitionPrior implements common.PartitionPrior{
	
	private FragmentPenalty fragmentPenalty;
	
	public FragmentPartitionPrior(FragmentPenalty fragmentPenalty){
		this.fragmentPenalty = fragmentPenalty;
	}
	
	@Override
	public double logPrior(ArrayList<ArrayList<Integer>> partition) {
		
		return -fragmentPenalty.penalty(partition);
	}
	
	
}

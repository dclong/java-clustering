package common;

import java.util.ArrayList;

public interface FragmentPenalty {
	public double penalty(ArrayList<ArrayList<Integer>> partition);
}

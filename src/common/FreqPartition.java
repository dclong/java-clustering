package common;

import java.io.IOException;
import java.util.Arrays;

public class FreqPartition {

	/**
	 * A 1-d array with class ID for each point.
	 */
	public final int[] partition;
	/**
	 * The frequency of the partition.
	 */
	private int freq;

	/**
	 * A constructor which make a shallow copy of partition.
	 * 
	 * @param partition
	 *            a 1-d array containing class ID for each DataPoint.
	 * @param freq
	 *            a positive integer.
	 */
	public FreqPartition(int[] partition, int freq) {
		this.partition = new int[partition.length];
		System.arraycopy(partition, 0, this.partition, 0, partition.length);
		this.freq = freq;
	}

	public FreqPartition(FreqPartition other){
		this(other.partition,other.freq);
	}
	/**
	 * Another constructor which make a shallow copy of partition and has a
	 * default frequency 1.
	 * 
	 * @param partition
	 */
	public FreqPartition(int[] partition) {
		this(partition, 1);
	}

	public void increaseFrequency(int by) {
		freq += by;
	}

	public void increaseFrequency() {
		freq++;
	}

	public int getFrequency() {// be careful, because no defensive copy is used
		return freq;
	}

	public int[] getPartition() {
		return partition;
	}

	public void write(java.io.DataOutputStream out) throws IOException {
		out.writeInt(freq);
		dclong.util.Arrays.write(partition, out);
	}
	
	public boolean equals(FreqPartition other){
		if(other==this){
			return true;
		}
		for(int i=partition.length-1; i>=0; --i){
			if(other.partition[i]!=partition[i]){
				return false;
			}
		}
		return true;
	}
	
	public static void reduce(java.util.ArrayList<FreqPartition> fps){
		FreqPartition currentFP, tempFP;
		for (int i = fps.size() - 1; i >= 0; --i) {
			currentFP = fps.get(i);
			for (int j = i - 1; j >= 0; --j) {
				tempFP = fps.get(j);
				if (currentFP.equals(tempFP)) {
					// combine
					currentFP.increaseFrequency(tempFP.getFrequency());
					fps.remove(j);
					--i;
				} 
			}
		}
	}
	
	/**
	 * Calculate the distance between this and other weighted by the frequency
	 * of other. Note that the distance defined here is not necessarily
	 * symmetric.
	 * 
	 * @param other
	 *            another object of FreqPartition.
	 * @return the distance between this and other.
	 */
	public long distance(FreqPartition other) {
		return other.freq * unweiDistance(other);
	}

	private long unweiDistance(FreqPartition other) {
		if (other == this) {
			return 0;
		}
		long d = 0;
		for (int i = partition.length - 1; i >= 1; --i) {
			for (int j = i - 1; j >= 0; --j) {
				if ((partition[i] == partition[j]) != (other.partition[i] == other.partition[j])) {
					d++;
				}
			}
		}
		return d;
	}
	
	public static FreqPartition mode(java.util.ArrayList<FreqPartition> fps){
		int size = fps.size();
		FreqPartition currentFP, maxFP = fps.get(size-1);
		for(int i=size-2; i>=0; --i){
			currentFP = fps.get(i);
			if(currentFP.freq>maxFP.freq){
				maxFP = currentFP;
			}
		}
		return new FreqPartition(maxFP);
	}
	
	/**
	 * Find the center of a list of FreqPartitions. Note that it combines same
	 * partitions, so the original ArrayList<FreqPartition> might be changed
	 * depending on whether there are same partitions or not.
	 * 
	 * @param fps
	 *            an ArrayList of FreqPartitions.
	 * @return an object of FreqPartition which has the smallest distance to
	 *         other FreqPartitions in the ArrayList.
	 */
	public static FreqPartition center(java.util.ArrayList<FreqPartition> fps,
			boolean combine) {
		if (combine) {
			return new FreqPartition(centerCombine(fps));
		}
		return new FreqPartition(centerUncombine(fps));
	}

	private static FreqPartition centerUncombine(
			java.util.ArrayList<FreqPartition> fps) {
		long currentDistance, minDistance = Long.MAX_VALUE;
		FreqPartition currentFP, center = null;
		int lastIndex = fps.size() - 1;
		for (int i = lastIndex; i >= 0; --i) {
			currentFP = fps.get(i);
			currentDistance = 0;
			for (int j = lastIndex; j >= 0; --j) {
				currentDistance += currentFP.distance(fps.get(j));
			}
			if (currentDistance < minDistance) {
				minDistance = currentDistance;
				center = currentFP;
			}
		}
		return center;
	}
	
//	public static void reduce(java.util.ArrayList<FreqPartition> fps){
//		FreqPartition currentFP;
//		for(int i=fps.size()-1; i>=0; --i){
//			currentFP = fps.get(i);
//		}
//	}
	
	private static FreqPartition centerCombine(
			java.util.ArrayList<FreqPartition> fps) {
		long d, currentDistance, minDistance = Long.MAX_VALUE;
		FreqPartition currentFP, tempFP, center = null;
		for (int i = fps.size() - 1; i >= 0; --i) {
			currentFP = fps.get(i);
			currentDistance = 0;
			for (int j = fps.size() - 1; j >= 0; --j) {
				if (j != i) {
					tempFP = fps.get(j);
					d = currentFP.distance(tempFP);
					if (d == 0) {
						// combine
						currentFP.increaseFrequency(tempFP.getFrequency());
						fps.remove(j);
						--i;
					} else {
						currentDistance += d;
					}
				}
			}
			if (currentDistance < minDistance) {
				minDistance = currentDistance;
				center = currentFP;
			}
		}
		return center;
	}

	@Override
	public String toString() {
		return "FreqPartition {partition=" + Arrays.toString(partition)
				+ ", freq=" + freq + "}";
	}

}

package common;

import java.util.ArrayList;


public class LinearSizePenalty implements SizePenalty{
	
	private double coefficient;
	/**
	 * 
	 * @param coefficient
	 */
	public LinearSizePenalty(double coefficient){
		this.coefficient = coefficient;
	}
	
	public double penalty(ArrayList<ArrayList<Integer>> partition){
		return coefficient * partition.size();
	}
}

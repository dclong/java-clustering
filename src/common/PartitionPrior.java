package common;

import java.util.ArrayList;

public interface PartitionPrior {	
	public double logPrior(ArrayList<ArrayList<Integer>> partition);
	
}

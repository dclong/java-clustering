package common;

import org.apache.commons.math.random.RandomDataImpl;


public class PoissonTuningDistribution implements TuningDistribution{
	
	private double mean;
	
	private RandomDataImpl rng;
	/**
	 * 
	 * @param rng
	 * @param mean
	 */
	public PoissonTuningDistribution(RandomDataImpl rng, double mean){
		this.rng = rng;
		this.mean = mean;
	}
	
	public long nextInt(){
		return rng.nextPoisson(mean) + 1;
	}
}

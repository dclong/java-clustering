package common;

import java.util.ArrayList;

public class PowerFragmentPenalty implements FragmentPenalty{
	
	private double power;
	
	private double coefficient;
	/**
	 * 
	 * @param power
	 * @param coefficient
	 */
	public PowerFragmentPenalty(double power,double coefficient){
		this.coefficient = coefficient;
		this.power = power;
	}
	
	public double penalty(ArrayList<ArrayList<Integer>> partition){
		double penalty = 0;
		int size = partition.size();
		for(int i=0; i<size; ++i){
			penalty += Math.pow(1d/partition.get(i).size(), power);
		}
		return coefficient * penalty;
	}
}

package common;

import java.util.ArrayList;

public interface SizePenalty {
	public double penalty(ArrayList<ArrayList<Integer>>partition);
}

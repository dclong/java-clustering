package complex1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;

import common.PartitionPrior;
import common.TuningDistribution;

public class MCMC {

	private int[] curArrayPartition;

	private int[] proArrayPartition;

	private double[] data;

	private double[] alphas;

	private double[] jumpWeights;

	private PartitionPrior partitionPrior;

	private TuningDistribution tuningDistribution;

	private double varianceRatio;

	private double variance;

	private RandomDataImpl rng;

	/**
	 * A constructor.
	 * 
	 * @param rng
	 *            an globally shared object of RandomDataImpl.
	 * 
	 * @param data
	 *            observations at data points.
	 * @param initArrayPartition
	 *            initial partition in array representation.
	 * @param initAlphas
	 *            initial values for alpha's.
	 * @param initJumpWeight
	 *            initial jumping weights.
	 * 
	 * @param partitionPrior
	 *            an object defining a log partition prior.
	 * 
	 * @param tuningDistribution
	 *            an object defining a tuning distribution.
	 * 
	 * @param variance
	 *            variance of the (normal) distribution of alpha's.
	 * @param varianceRatio
	 *            the ratio of variance of the (normal) distribution of alpha's
	 *            to the variance of the (normal) distribution of parameters
	 *            associated with subsets.
	 */
	public MCMC(RandomDataImpl rng, double[] data, int[] initArrayPartition,
			double[] initAlphas, double[] initJumpWeights,
			PartitionPrior partitionPrior,
			TuningDistribution tuningDistribution, double variance,
			double varianceRatio) {
		this.rng = rng;
		setData(data);
		setInitialPartition(initArrayPartition);
		setInitialAlphas(initAlphas);
		setPartitionPrior(partitionPrior);
		setVariance(variance);
		setVarianceRatio(varianceRatio);
		setJumpWeights(initJumpWeights);
		setTuningDistribution(tuningDistribution);
		// ---------------------------------------------------------
		initializeOthers();
	}

	public void setVarianceRatio(double varianceRatio) {
		this.varianceRatio = varianceRatio;
	}

	public void setVariance(double variance) {
		this.variance = variance;
	}

	public void setTuningDistribution(TuningDistribution tuningDistribution) {
		this.tuningDistribution = tuningDistribution;
	}

	public void setPartitionPrior(PartitionPrior partitionPrior) {
		this.partitionPrior = partitionPrior;
	}

	public void setInitialAlphas(double[] initAlphas) {
		// copy initial parameters
		if (alphas == null || alphas.length != initAlphas.length) {
			alphas = new double[initAlphas.length];
		}
		System.arraycopy(initAlphas, 0, alphas, 0, initAlphas.length);
	}

	private void setData(double[] data) {
		if (this.data == null || this.data.length != data.length) {
			this.data = new double[data.length];
		}
		System.arraycopy(data, 0, this.data, 0, data.length);
	}

	public void setInitialPartition(int[] partition) {
		if (curArrayPartition == null
				|| curArrayPartition.length != partition.length) {
			curArrayPartition = new int[partition.length];
		}
		System.arraycopy(partition, 0, curArrayPartition, 0, partition.length);
		if (proArrayPartition == null
				|| proArrayPartition.length != partition.length) {
			proArrayPartition = new int[partition.length];
		}
		System.arraycopy(partition, 0, proArrayPartition, 0, partition.length);
		curListPartition = dclong.util.Arrays.splitArray(curArrayPartition);
		proListPartition = dclong.util.Arrays.splitArray(proArrayPartition);
	}

	private void warmupNoAdjust(int step) {
		acceptCount = 0;
		dclong.util.Arrays.divide(probability, jumpWeights,
				dclong.util.Arrays.sum(jumpWeights));
		for (int i = 0; i < step; ++i) {
//			System.out.println("\nStep " + i + ":");
			drawPartitionGivenAlphas();
			drawAlphasGivenPartition();
		}
		jumpRatio = acceptCount / (double) step;
	}

	private void warmupAutoAdjust(int step) {
		acceptCount = 0;
		for (int i = 0; i < step; ++i) {
//			System.out.println("\nStep " + i + ":");
			probability = dclong.util.Arrays.divide(jumpWeights,
					dclong.util.Arrays.sum(jumpWeights));
			drawPartitionGivenAlphas();
			adjustJumpWeights(acceptanceRatio);
			drawAlphasGivenPartition();
		}
		jumpRatio = acceptCount / (double) step;
	}

	public void warmup(int step, boolean autoAdjust) {
		// initialize movedPoints
		if (movedPoints == null) {
			movedPoints = new ArrayList<Integer>();
		}
		// initialize log posterior
		currentLogPosterior = logPosteriorPartitionGivenAlphas();
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if (autoAdjust) {
			warmupAutoAdjust(step);
		} else {
			warmupNoAdjust(step);
		}
	}

	public void run(int step) {
		// initialize fps
		int capacity = (int) (step * 0.25);
		if (fps == null) {
			fps = new ArrayList<common.FreqPartition>(capacity);
		} else {
			fps.ensureCapacity(capacity);
		}
		resetFps();
		// no need of movedPoints when running mcmc
		movedPoints = null;
		// initialize log posterior of partition given alphas
		currentLogPosterior = logPosteriorPartitionGivenAlphas();
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		dclong.util.Arrays.divide(probability, jumpWeights,
				dclong.util.Arrays.sum(jumpWeights));
		boolean accept;
		acceptCount = 0;
		for (int i = 0; i < step; ++i) {
			accept = drawPartitionGivenAlphas();
			if (accept) {
				fps.add(new common.FreqPartition(proArrayPartition));
			} else {
				fps.get(fps.size() - 1).increaseFrequency();
			}
			drawAlphasGivenPartition();
		}
		jumpRatio = acceptCount / (double) step;
	}

	public double[] getProbability() {
		return probability;
	}

	public ArrayList<common.FreqPartition> getFps() {
		return fps;
	}

	public double[] getJumpWeights() {
		return jumpWeights;
	}

	public void writeFP(String file) {
		java.io.DataOutputStream out = null;
		try {
			out = new java.io.DataOutputStream(new java.io.FileOutputStream(
					file));
		} catch (FileNotFoundException e) {
			System.out.println("Creating fileOutputStream object from file "
					+ file + " failed.");
			e.printStackTrace();
		}
		int size = fps.size();
		for (int i = 0; i < size; ++i) {
			try {
				fps.get(i).write(out);
			} catch (IOException e) {
				System.out.println("Writing data into file " + file
						+ " failed.");
				e.printStackTrace();
			}
		}
	}

	public double getJumpRatio() {
		return jumpRatio;
	}

	private void resetFps() {
		fps.clear();
		fps.add(new common.FreqPartition(curArrayPartition));
	}

	public void setJumpWeights(double[] weights) {
		if (jumpWeights == null) {
			jumpWeights = new double[weights.length];
		}
		System.arraycopy(weights, 0, this.jumpWeights, 0, weights.length);
		probability = new double[jumpWeights.length];
	}

	private void drawAlphasGivenPartition() {
		int partitionSize = curListPartition.size();
		for (int i = 0; i < partitionSize; ++i) {
			ArrayList<Integer> set = curListPartition.get(i);
			drawAlphasGivenPartition(set);
		}
//		System.out.println("Alphas:");
//		dclong.io.Print.print(alphas);
//		System.out.print("\n");
//		System.out.println("Observations: ");
//		dclong.io.Print.print(data);
//		System.out.print("\n");
	}

	private void drawAlphasGivenPartition(ArrayList<Integer> set) {
		double sum = dclong.util.Arrays.sum(alphas, set);
		int size = set.size();
		double adjustedSize = size + varianceRatio;
		double adjustedSizeMinusOne = adjustedSize - 1;
		for (int i = 0; i < size; ++i) {
			int index = set.get(i);
			double mu1 = (sum - alphas[index]) / adjustedSizeMinusOne;
			double var1 = adjustedSize / adjustedSizeMinusOne * variance;
			double mu2 = data[index];
			double var2 = 1;
			alphas[index] = drawAlpha(mu1, var1, mu2, var2);
		}
	}

	private double drawAlpha(double mu1, double var1, double mu2, double var2) {
		double invVar1 = 1d / var1;
		double invVar2 = 1d / var2;
		double invVar = invVar1 + invVar2;
		double var = 1d / invVar;
		double mu = (mu1 * invVar1 + mu2 * invVar2) / invVar;
		return rng.nextGaussian(mu, Math.sqrt(var));
	}

	private boolean drawPartitionGivenAlphas() {
		proposePartition();
		double proposedLogPosterior = logPosteriorPartitionGivenAlphas();
		double logPosteriorDiff = proposedLogPosterior - currentLogPosterior;
//		System.out.print("logPosteriorDiff: " + logPosteriorDiff + "  ");
		if (logPosteriorDiff >= 0) {
			// accept
//			System.out.print("r>1  ");
			acceptanceRatio = 1;
			acceptPartition(proposedLogPosterior);
			return true;
		} else {
			//force the chain to jump
			logPosteriorDiff = Math.max(logPosteriorDiff, -4.6);
			acceptanceRatio = Math.exp(logPosteriorDiff);
//			System.out.print("r=" + acceptanceRatio + "  ");
			if (rng.nextUniform(0, 1) <= acceptanceRatio) {
				// accept
				acceptPartition(proposedLogPosterior);
				return true;
			} else {
				// reject
				rejectPartition();
				return false;
			}
		}
	}

	/**
	 * Accept the proposed partition. Related variables are updated, except fps
	 * which is the ArrayList for saving posterior draws. So after calling this
	 * method, you have to manually add the proposed partition into fps. This is
	 * mainly for the concern of convenience for writing a warm-up method.
	 * 
	 * @param proposedPosterior
	 */
	private void acceptPartition(double proposedPosterior) {
		// update current partition
		System.arraycopy(proArrayPartition, 0, curArrayPartition, 0,
				data.length);
		curListPartition = dclong.util.Arrays.splitArray(curArrayPartition);
		// update posterior
		currentLogPosterior = proposedPosterior;
		acceptCount++;
//		System.out.println("ACCEPTED!");
	}

	private void adjustJumpWeights(double total) {
		int size = movedPoints.size();
		double inc = total / size;
		for (int i = size - 1; i >= 0; --i) {
			jumpWeights[movedPoints.get(i)] += inc;
		}
	}

	private void rejectPartition() {
		// reset proposed partition
		System.arraycopy(curArrayPartition, 0, proArrayPartition, 0,
				data.length);
		proListPartition = dclong.util.Arrays.splitArray(proArrayPartition);
//		System.out.println("REJECTED!");
	}

	// private int[] listToArray(){
	// int[] array = new int[n];
	// int size = proListPartition.size();
	// for(int i=0; i<size; i++){
	// ArrayList<Integer> currentList = proListPartition.get(i);
	// int step = currentList.size();
	// int g = currentList.get(0);
	// for(int j=0; j<step; j++){
	// array[currentList.get(j)] = g;
	// }
	// }
	// return array;
	// }

	private void proposePartition() {
		long step = tuningDistribution.nextInt();
		if (movedPoints != null) {
			movedPoints.clear();
			for (long i = 0; i < step; ++i) {
				proposeOneStepTrack();
			}
		} else {
			for (long i = 0; i < step; ++i) {
				proposeOneStepNoTrack();
			}
		}
//		System.out.println("Moved points: " + step);
//
//		System.out.println("Current partition in list representation:");
//		System.out.println(curListPartition);
//		System.out.println("Currrent partition in array representation:");
//		dclong.io.Print.print(curArrayPartition);
//		System.out.print("\n");
//		System.out.println("Proposed partition in list representation:");
//		System.out.println(proListPartition);
//		System.out.println("Proposed partition in array representation:");
//		dclong.io.Print.print(proArrayPartition);
//		System.out.print("\n");
	}

	private void proposeOneStepTrack() {
		// select a point and push it to movedPoints
		int srcWithinIndex = dclong.stat.Random.sample(rng, probability);
		proposeOneStepCore(srcWithinIndex);
		movedPoints.add(srcWithinIndex);
	}

	private void proposeOneStepCore(int srcWithinIndex) {
		// find the source class
		int partitionSize = proListPartition.size();
		for (int srcClassIndex = 0; srcClassIndex < partitionSize; ++srcClassIndex) {
			ArrayList<Integer> srcClass = proListPartition.get(srcClassIndex);
			int srcClassSize = srcClass.size();
			if (srcWithinIndex < srcClassSize) {// point to move is found
				int moveId = srcClass.get(srcWithinIndex);
				if (srcClassSize == 1) {// to be moved to an existing class
					// select a destination class randomly

					int desClassIndex = (int) rng.nextUniform(0,
							partitionSize - 1);
					if (desClassIndex == srcClassIndex) {
						desClassIndex = partitionSize - 1;
					}
					ArrayList<Integer> desClass = proListPartition
							.get(desClassIndex);
					// move the point
					int desWithinIndex = findDesWithinIndex(desClass, moveId);
					desClass.add(desWithinIndex, moveId);
					proListPartition.remove(srcClassIndex);
					// update proposed partition in array representation
					updateDesClass(desClass, desWithinIndex, moveId);
				} else {// move to an existing class or a new singleton
					int desClassIndex = (int) rng.nextUniform(0, partitionSize);
					if (desClassIndex == srcClassIndex) {
						// add a new singleton class
						ArrayList<Integer> singleton = new ArrayList<Integer>();
						singleton.add(moveId);
						proListPartition.add(singleton);
						srcClass.remove(srcWithinIndex);
						// update proposed partition in array representation
						updateSrcClass(srcClass, srcWithinIndex);
						updateDesClass(singleton, 0, moveId);
					} else {
						// move to an existing class
						ArrayList<Integer> desClass = proListPartition
								.get(desClassIndex);
						int desWithinIndex = findDesWithinIndex(desClass,
								moveId);
						desClass.add(desWithinIndex, moveId);
						srcClass.remove(srcWithinIndex);
						// update proposed partition in array representation
						updateSrcClass(srcClass, srcWithinIndex);
						updateDesClass(desClass, desWithinIndex, moveId);
					}
				}
				break;
			} else {
				srcWithinIndex -= srcClassSize;
			}
		}
	}

	private void proposeOneStepNoTrack() {
		// select a point and push it to movedPoints
		int srcWithinIndex = dclong.stat.Random.sample(rng, probability);
		proposeOneStepCore(srcWithinIndex);
	}

	private void updateSrcClass(ArrayList<Integer> srcSet, int srcWithinIndex) {
		if (srcWithinIndex == 0) {
			int size = srcSet.size();
			int newName = srcSet.get(0);
			for (int i = 0; i < size; ++i) {
				proArrayPartition[srcSet.get(i)] = newName;
			}
		}
	}

	private void updateDesClass(ArrayList<Integer> desSet, int desWithinIndex,
			int moveId) {
		if (desWithinIndex == 0) {
			int size = desSet.size();
			for (int i = 0; i < size; ++i) {
				proArrayPartition[desSet.get(i)] = moveId;
			}
		} else {
			proArrayPartition[moveId] = desSet.get(0);
		}
	}

	private int findDesWithinIndex(ArrayList<Integer> desClass, int id) {
		int desClassSize = desClass.size();
		int desWithinIndex;
		for (desWithinIndex = 0; desWithinIndex < desClassSize; ++desWithinIndex) {
			if (desClass.get(desWithinIndex) > id) {
				break;
			}
		}
		return desWithinIndex;
	}

	private double logPosteriorPartitionGivenAlphas() {
		double temp, logPosterior, classSize;
		int size = proListPartition.size();
		logPosterior = 0.5 * size * Math.log(varianceRatio);
		ArrayList<Integer> set;
		for (int i = 0; i < size; ++i) {
			set = proListPartition.get(i);
			classSize = set.size();
			temp = dclong.util.Arrays.sum(alphas, set);
			temp *= temp;
			temp /= 2 * variance * (classSize + varianceRatio);
			temp -= 0.5 * Math.log(varianceRatio + classSize);
			logPosterior += temp;
		}
		logPosterior += partitionPrior.logPrior(proListPartition);
		return logPosterior;
	}

	// private double logPartitionPrior(ArrayList<ArrayList<Integer>>partition)
	// {
	// return -partitionComplexity(partition);
	// }

	private void initializeOthers() {

	}

	private int acceptCount;

	private double jumpRatio;

	private double currentLogPosterior;

	private ArrayList<ArrayList<Integer>> curListPartition;
	private ArrayList<ArrayList<Integer>> proListPartition;

	private ArrayList<common.FreqPartition> fps;

	private ArrayList<Integer> movedPoints;

	private double acceptanceRatio;

	private double[] probability;
}

package complex1;

import java.util.ArrayList;

public class TestCenter {
	public static void main(String[] args){
		int ap1[] = {0,0,0,0,0,0};
		int ap2[] = {0,0,0,2,3,3};
		int ap3[] = {0,0,2,2,4,4};
		int ap4[] = {0,0,0,3,3,3};
		int ap5[] = {0,0,2,2,4,4};
		int ap6[] = {0,0,2,2,4,4};
		
		ArrayList<common.FreqPartition> fps = new ArrayList<common.FreqPartition>();
		fps.add(new common.FreqPartition(ap1));
		fps.add(new common.FreqPartition(ap2));
		fps.add(new common.FreqPartition(ap3));
		fps.add(new common.FreqPartition(ap4));
		fps.add(new common.FreqPartition(ap5));
		fps.add(new common.FreqPartition(ap6));
		//--------------------------------------------------
		dclong.util.Timer timer = new dclong.util.Timer();
		timer.start();
		common.FreqPartition.reduce(fps);
		timer.stop();
		timer.printSeconds("finding center");
		System.out.println(fps);
	}
	
}

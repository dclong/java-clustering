package complex1;

import java.io.IOException;

public class TestData {
	public static void main(String[] args) throws IOException{
		String file = "/home/adu/Dropbox/research/vardeman/mc-on-partition/data/jdata/tap.bin";
		dclong.util.Arrays.print(dclong.io.BinaryReader.readInt(file, 0, 0, 20));
		System.out.println("\n");
		file = "/home/adu/Dropbox/research/vardeman/mc-on-partition/data/jdata/data2.bin";
		dclong.util.Arrays.print(dclong.io.BinaryReader.readDouble(file, 0, 0, 20));
		file = "/home/adu/Dropbox/research/vardeman/mc-on-partition/data/jdata/locs2.bin";
		System.out.println("\n");
		dclong.util.Arrays.print(dclong.io.BinaryReader.readDouble(file,0,0,20,2));
	}
}

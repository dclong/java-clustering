package complex1;

public class TestDistance {
	public static void main(String[] args){
		int[] ap1 = {0,0,0,3,4,5};
		int[] ap2 = {0,0,2,2,2,2};
		common.FreqPartition fp1 = new common.FreqPartition(ap1);
		common.FreqPartition fp2 = new common.FreqPartition(ap2);
		long d = fp1.distance(fp2);
		System.out.println(d);
	}
}

package normal2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;
import org.apache.commons.math.special.Gamma;

public class MCMC {

	private int[] curArrayPartition;

	private int[] proArrayPartition;

	private double[] data;

	private double[] jumpWeights;

	private common.PartitionPrior partitionPrior;

	private common.TuningDistribution tuningDistribution;

	double alpha;
	
	double beta;

	private RandomDataImpl rng;

	/**
	 * A constructor.
	 * 
	 * @param rng
	 *            an globally shared object of RandomDataImpl.
	 * 
	 * @param data
	 *            observations at data points.
	 * @param initArrayPartition
	 *            initial partition in array representation.
	 * @param initAlphas
	 *            initial values for alpha's.
	 * @param initJumpWeight
	 *            initial jumping weights.
	 * 
	 * @param partitionPrior
	 *            an object defining a log partition prior.
	 * 
	 * @param tuningDistribution
	 *            an object defining a tuning distribution.
	 * 
	 * @param variance
	 *            variance of the (normal) distribution of alpha's.
	 * @param varianceRatio
	 *            the ratio of variance of the (normal) distribution of alpha's
	 *            to the variance of the (normal) distribution of parameters
	 *            associated with subsets.
	 */
	public MCMC(RandomDataImpl rng, double[] data, int[] initArrayPartition,
			 double[] initJumpWeights,
			common.PartitionPrior partitionPrior,
			common.TuningDistribution tuningDistribution, double variance,
			double varianceRatio) {
		this.rng = rng;
		setData(data);
		setInitialPartition(initArrayPartition);
		setJumpWeights(initJumpWeights);
		setPartitionPrior(partitionPrior);
		setAlpha(variance);
		setBeta(varianceRatio);
		setTuningDistribution(tuningDistribution);
		// ---------------------------------------------------------
		initializeOthers();
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public void setBeta(double beta) {
		this.beta = beta;
	}

	public void setTuningDistribution(common.TuningDistribution tuningDistribution) {
		this.tuningDistribution = tuningDistribution;
	}

	public void setPartitionPrior(common.PartitionPrior partitionPrior) {
		this.partitionPrior = partitionPrior;
	}


	private void setData(double[] data) {
		if (this.data == null || this.data.length != data.length) {
			this.data = new double[data.length];
		}
		System.arraycopy(data, 0, this.data, 0, data.length);
	}

	public void setInitialPartition(int[] partition) {
		if (curArrayPartition == null
				|| curArrayPartition.length != partition.length) {
			curArrayPartition = new int[partition.length];
		}
		System.arraycopy(partition, 0, curArrayPartition, 0, partition.length);
		if (proArrayPartition == null
				|| proArrayPartition.length != partition.length) {
			proArrayPartition = new int[partition.length];
		}
		System.arraycopy(partition, 0, proArrayPartition, 0, partition.length);
		proListPartition = dclong.util.Arrays.splitArray(proArrayPartition);
	}

	private void warmupNoAdjust(int step) {
		acceptCount = 0;
		dclong.util.Arrays.divide(probability, jumpWeights,
				dclong.util.Arrays.sum(jumpWeights));
		for (int i = 0; i < step; ++i) {
			drawPartition();
		}
		jumpRatio = acceptCount / (double) step;
	}

//	private void warmAdjust(int step,int type){
//		
//	}
	
//	private void warmupAutoAdjust1(int step) {
//		acceptCount = 0;
//		for (int i = 0; i < step; ++i) {
////			System.out.println("\nStep " + i + ":");
//			probability = dclong.util.Arrays.divide(jumpWeights,
//					dclong.util.Arrays.sum(jumpWeights));
//			drawPartitionGivenAlphas();
//			adjustJumpWeights(acceptanceRatio);
//			drawAlphasGivenPartition();
//		}
//		jumpRatio = acceptCount / (double) step;
//	}
	
//	private void warmAutoAdjust2(int step){
//		
//	}
	
	public void setJumpWeights(double[] weights) {
		if (jumpWeights == null) {
			jumpWeights = new double[weights.length];
		}
		System.arraycopy(weights, 0, this.jumpWeights, 0, weights.length);
		probability = new double[jumpWeights.length];
	}

	public void warmup(int step, boolean autoAdjust) {
		// initialize movedPoints
		if (movedPoints == null) {
			movedPoints = new ArrayList<Integer>();
		}
		// initialize log posterior
		currentLogPosterior = logPosterior();
		// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		if (autoAdjust) {
			
		} else {
			warmupNoAdjust(step);
		}
	}

	public void run(int step) {
		// initialize fps
		int capacity = (int) (step * 0.25);
		if (fps == null) {
			fps = new ArrayList<common.FreqPartition>(capacity);
		} else {
			fps.ensureCapacity(capacity);
		}
		resetFps();
		// no need of movedPoints when running mcmc
		movedPoints = null;
		// initialize log posterior of partition given alphas
		currentLogPosterior = logPosterior();
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		dclong.util.Arrays.divide(probability, jumpWeights,
				dclong.util.Arrays.sum(jumpWeights));
		boolean accept;
		acceptCount = 0;
		for (int i = 0; i < step; ++i) {
			accept = drawPartition();
			if (accept) {
				fps.add(new common.FreqPartition(proArrayPartition));
			} else {
				fps.get(fps.size() - 1).increaseFrequency();
			}
		}
		jumpRatio = acceptCount / (double) step;
	}

	public double[] getProbability() {
		return probability;
	}

	public ArrayList<common.FreqPartition> getFps() {
		return fps;
	}

	public double[] getJumpWeights() {
		return jumpWeights;
	}

	public void writeFP(String file) {
		java.io.DataOutputStream out = null;
		try {
			out = new java.io.DataOutputStream(new java.io.FileOutputStream(
					file));
		} catch (FileNotFoundException e) {
			System.out.println("Creating fileOutputStream object from file "
					+ file + " failed.");
			e.printStackTrace();
		}
		int size = fps.size();
		for (int i = 0; i < size; ++i) {
			try {
				fps.get(i).write(out);
			} catch (IOException e) {
				System.out.println("Writing data into file " + file
						+ " failed.");
				e.printStackTrace();
			}
		}
	}

	public double getJumpRatio() {
		return jumpRatio;
	}

	private void resetFps() {
		fps.clear();
		fps.add(new common.FreqPartition(curArrayPartition));
	}

	

	

	

	

	private boolean drawPartition() {
		proposePartition();
		double proposedLogPosterior = logPosterior();
		double logPosteriorDiff = proposedLogPosterior - currentLogPosterior;
		if (logPosteriorDiff >= 0) {
			// accept
			acceptanceRatio = 1;
			acceptPartition(proposedLogPosterior);
			return true;
		} else {
			acceptanceRatio = Math.exp(logPosteriorDiff);
			if (rng.nextUniform(0, 1) <= acceptanceRatio) {
				// accept
				acceptPartition(proposedLogPosterior);
				return true;
			} else {
				// reject
				rejectPartition();
				return false;
			}
		}
	}

	/**
	 * Accept the proposed partition. Related variables are updated, except fps
	 * which is the ArrayList for saving posterior draws. So after calling this
	 * method, you have to manually add the proposed partition into fps. This is
	 * mainly for the concern of convenience for writing a warm-up method.
	 * 
	 * @param proposedPosterior
	 */
	private void acceptPartition(double proposedPosterior) {
		// update current partition
		System.arraycopy(proArrayPartition, 0, curArrayPartition, 0,
				data.length);
		// update posterior
		currentLogPosterior = proposedPosterior;
		acceptCount++;
	}

	private void rejectPartition() {
		// reset proposed partition
		System.arraycopy(curArrayPartition, 0, proArrayPartition, 0,
				data.length);
		proListPartition = dclong.util.Arrays.splitArray(proArrayPartition);
	}

	// private int[] listToArray(){
	// int[] array = new int[n];
	// int size = proListPartition.size();
	// for(int i=0; i<size; i++){
	// ArrayList<Integer> currentList = proListPartition.get(i);
	// int step = currentList.size();
	// int g = currentList.get(0);
	// for(int j=0; j<step; j++){
	// array[currentList.get(j)] = g;
	// }
	// }
	// return array;
	// }

	private void proposePartition() {
		long step = tuningDistribution.nextInt();
		if (movedPoints != null) {
			movedPoints.clear();
			for (long i = 0; i < step; ++i) {
				proposeOneStepTrack();
			}
		} else {
			for (long i = 0; i < step; ++i) {
				proposeOneStepNoTrack();
			}
		}
	}

	private void proposeOneStepTrack() {
		// select a point and push it to movedPoints
		int srcWithinIndex = dclong.stat.Random.sample(rng, probability);
		proposeOneStepCore(srcWithinIndex);
		movedPoints.add(srcWithinIndex);
	}

	private void proposeOneStepCore(int srcWithinIndex) {
		// find the source class
		int partitionSize = proListPartition.size();
		for (int srcClassIndex = 0; srcClassIndex < partitionSize; ++srcClassIndex) {
			ArrayList<Integer> srcClass = proListPartition.get(srcClassIndex);
			int srcClassSize = srcClass.size();
			if (srcWithinIndex < srcClassSize) {// point to move is found
				int moveId = srcClass.get(srcWithinIndex);
				if (srcClassSize == 1) {// to be moved to an existing class
					// select a destination class randomly

					int desClassIndex = (int) rng.nextUniform(0,
							partitionSize - 1);
					if (desClassIndex == srcClassIndex) {
						desClassIndex = partitionSize - 1;
					}
					ArrayList<Integer> desClass = proListPartition
							.get(desClassIndex);
					// move the point
					int desWithinIndex = findDesWithinIndex(desClass, moveId);
					desClass.add(desWithinIndex, moveId);
					proListPartition.remove(srcClassIndex);
					// update proposed partition in array representation
					updateDesClass(desClass, desWithinIndex, moveId);
				} else {// move to an existing class or a new singleton
					int desClassIndex = (int) rng.nextUniform(0, partitionSize);
					if (desClassIndex == srcClassIndex) {
						// add a new singleton class
						ArrayList<Integer> singleton = new ArrayList<Integer>();
						singleton.add(moveId);
						proListPartition.add(singleton);
						srcClass.remove(srcWithinIndex);
						// update proposed partition in array representation
						updateSrcClass(srcClass, srcWithinIndex);
						updateDesClass(singleton, 0, moveId);
					} else {
						// move to an existing class
						ArrayList<Integer> desClass = proListPartition
								.get(desClassIndex);
						int desWithinIndex = findDesWithinIndex(desClass,
								moveId);
						desClass.add(desWithinIndex, moveId);
						srcClass.remove(srcWithinIndex);
						// update proposed partition in array representation
						updateSrcClass(srcClass, srcWithinIndex);
						updateDesClass(desClass, desWithinIndex, moveId);
					}
				}
				break;
			} else {
				srcWithinIndex -= srcClassSize;
			}
		}
	}

	private void proposeOneStepNoTrack() {
		// select a point and push it to movedPoints
		int srcWithinIndex = dclong.stat.Random.sample(rng, probability);
		proposeOneStepCore(srcWithinIndex);
	}

	private void updateSrcClass(ArrayList<Integer> srcSet, int srcWithinIndex) {
		if (srcWithinIndex == 0) {
			int size = srcSet.size();
			int newName = srcSet.get(0);
			for (int i = 0; i < size; ++i) {
				proArrayPartition[srcSet.get(i)] = newName;
			}
		}
	}

	private void updateDesClass(ArrayList<Integer> desSet, int desWithinIndex,
			int moveId) {
		if (desWithinIndex == 0) {
			int size = desSet.size();
			for (int i = 0; i < size; ++i) {
				proArrayPartition[desSet.get(i)] = moveId;
			}
		} else {
			proArrayPartition[moveId] = desSet.get(0);
		}
	}

	private int findDesWithinIndex(ArrayList<Integer> desClass, int id) {
		int desClassSize = desClass.size();
		int desWithinIndex;
		for (desWithinIndex = 0; desWithinIndex < desClassSize; ++desWithinIndex) {
			if (desClass.get(desWithinIndex) > id) {
				break;
			}
		}
		return desWithinIndex;
	}

	private double logPosterior() {
		int partitionSize = proListPartition.size();
		double logPosterior = partitionSize * (alpha*Math.log(beta)+0.5*Math.log(2*Math.PI)-Gamma.logGamma(alpha));
		ArrayList<Integer> set;
		for(int i=0; i<partitionSize; ++i){
			set = proListPartition.get(i);
			double classSize = set.size();
			double temp = 0.5*(classSize-1) + alpha;
			logPosterior += Gamma.logGamma(temp);
			logPosterior -= temp*Math.log(beta+0.5*sumOfSquares(set));
			logPosterior -= 0.5*Math.log(classSize);
		}
		logPosterior += partitionPrior.logPrior(proListPartition);
		return logPosterior;
	}
	
	private double sumOfSquares(ArrayList<Integer> set){
		double average = 0;
		int size = set.size();
		for(int i=0; i<size; ++i){
			average += data[set.get(i)];
		}
		average /= size;
		double ss = 0;
		double temp;
		for(int i=0; i<size; ++i){
			temp = (data[set.get(i)] - average);
			ss += temp*temp;
		}
		return ss;
	}
	
	// private double logPartitionPrior(ArrayList<ArrayList<Integer>>partition)
	// {
	// return -partitionComplexity(partition);
	// }

	private void initializeOthers() {

	}

	private int acceptCount;

	private double jumpRatio;

	private double currentLogPosterior;

	private ArrayList<ArrayList<Integer>> proListPartition;

	private ArrayList<common.FreqPartition> fps;

	private ArrayList<Integer> movedPoints;

	private double acceptanceRatio;

	private double[] probability;
}



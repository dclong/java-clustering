package orientation1;

public enum NeighborType {
	FOUR(4,1,1),
	EIGHT(8,2,1), 
	TWELVE(12,4,2),
	TWENTY_FOUR(24,8,2),
	TWENTY_EIGHT(28,9,3);
	/**
	 * The (maximum) number of neighbors. 
	 * Some points (e.g. points at corners) don't have so many points.
	 */
	private final int number;
	/**
	 * The (squared) distance defining the neighbors. 
	 */
	private final int distance;
	/**
	 * The radius of the smallest square containing all neighbors.
	 * Where the radius is defined as half the side length.
	 */
	private final int radius;
	/**
	 * A private constructor.
	 * @param aNumber the (maximum) number of neighbors.
	 * @param aDistance the distance defining neighbors.
	 * @param aRadius the radius of the smallest square containing all neighbors.
	 */
	private NeighborType(int aNumber, int aDistance, int aRadius){
		number = aNumber;
		distance = aDistance;
		radius = aRadius;
	}
	/**
	 * Get the (maximum) number of neighbors.
	 * @return the (maximum) number of neighbors.
	 */
	public int getNumber(){
		return number;
	}
	/**
	 * Get the distance defining neighbors.
	 * @return the distance defining neighbors.
	 */
	public int getDistance(){
		return distance;
	}
	/**
	 * Get the radius of the smallest square containing all neighbors. 
	 * @return the radius of the smallest square containing all neighbors.
	 */
	public int getRadious(){
		return radius;
	}
}
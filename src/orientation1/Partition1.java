package orientation1;

import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;

public class Partition1 {
	/**
	 * A 2-d array indicates the partition. Grids with the same integer are in
	 * the same group.
	 */
	private int[][] part;
	/**
	 * Observed Data on grids.
	 */
	private double[][] data;
	/**
	 * A random number generator shared among all objects of Partition.
	 */
	private RandomDataImpl rng;
	/**
	 * The following two coefficients defines alpha (which is the rate to
	 * generate a singleton) as alpha = 1/(b1+b2*n), where n is the number of
	 * neighbors.
	 */
	private double b1;
	private double b2;
	/**
	 * Parameters for random data on grids.
	 */
	private double[][] para;
	/**
	 * Neighbor type, either 4-neighbors or 8-neighbors.
	 */
	private NeighborType ntype;
	/**
	 * Jump type, either all can jump or only points on boundary can jump.
	 */
	private JumpType jtype;
	/**
	 * Boundary in the partition.
	 */
	private ArrayList<GridIndex> boundary;
	/**
	 * An ArrayList for storing frequency results.
	 */
	private ArrayList<PartitionFrequency> pfList;

	public Partition1(RandomDataImpl aRNG, double[][] aData, int[][] aPart,
			double aB1, double aB2, NeighborType aNType, JumpType aJType) {
		b1 = aB1;
		b2 = aB2;
		ntype = aNType;
		// initialize ArrayLists
		pfList = new ArrayList<PartitionFrequency>();
		boundary = new ArrayList<GridIndex>();
		setJumpType(aJType);
		rng = aRNG;
		// initialize data arrays
		data = new double[aData.length][aData[0].length];
		// copy data
		copyArray(aData, data);
		// initialize partition array
		part = new int[aPart.length][aPart[0].length];
		// copy partition
		copyArray(aPart, part);
		// initialize parameter array
		para = new double[data.length][data[0].length];
	}

	public Partition1(RandomDataImpl aRNG, double[][] aData, 
			double aB1, double aB2, NeighborType aNType, JumpType aJType) {
		b1 = aB1;
		b2 = aB2;
		ntype = aNType;
		// initialize ArrayLists
		pfList = new ArrayList<PartitionFrequency>();
		boundary = new ArrayList<GridIndex>();
		setJumpType(aJType);
		rng = aRNG;
		// initialize data arrays
		data = new double[aData.length][aData[0].length];
		// copy data
		copyArray(aData, data);
		// initialize partition array
		part = new int[data.length][data[0].length];
		// initialize parameter array
		para = new double[data.length][data[0].length];
	}
	
	public void setPartition(int[][] aPart){
		copyArray(aPart,part);
	}
	/**
	 * Set new data on grids.
	 * @param aData
	 */
	public void setData(double[][] aData){
		copyArray(aData,data);
	}
	/**
	 * Copy a 2-d int array.
	 * 
	 * @param from
	 * @param to
	 */
	private void copyArray(int[][] from, int[][] to) {
		for (int i = 0; i < from.length; i++) {
			for (int j = 0; j < from[0].length; j++) {
				to[i][j] = from[i][j];
			}
		}
	}

	/**
	 * Copy a 2-d double array.
	 * 
	 * @param from
	 * @param to
	 */
	private void copyArray(double[][] from, double[][] to) {
		for (int i = 0; i < from.length; i++) {
			for (int j = 0; j < from[0].length; j++) {
				to[i][j] = from[i][j];
			}
		}
	}

	/**
	 * Randomly select an element of the current partition, and let it jump.
	 */
	public void jump() {
		GridIndex gi;
		if (jtype == JumpType.ALL) {
			// randomly choose an index combination
			int rowIndex = rng.nextInt(0, part.length - 1);
			int colIndex = rng.nextInt(0, part[0].length - 1);
			gi = new GridIndex(rowIndex, colIndex);
		} else {
			// randomly choose an index combination on boundary
			int b = rng.nextInt(0, boundary.size());
			gi = boundary.get(b);
		}
		// find gi's neighbors
		GridIndex[] neis = getNeighbors(gi);
		// calculate weights

	}

	public void updateParameter() {

	}

	public void updateBoundary() {

	}

	public NeighborType getNeighborType() {
		return ntype;
	}

	public void setB1(double aB1) {
		b1 = aB1;
	}

	public void setB2(double aB2) {
		b2 = aB2;
	}

	public void setAlpha(double aB1, double aB2) {
		setB1(aB1);
		setB2(aB2);
	}

	public void setNeighborType(NeighborType aNType) {
		ntype = aNType;
	}

	public void setJumpType(JumpType aJType) {
		jtype = aJType;
		if (jtype == JumpType.BOUNDARY) {
			findBoundary();
		} else {
			boundary.clear();
		}
	}

	private void findBoundary() {
		for (int i = 0; i < part.length; i++) {
			for (int j = 0; j < part.length; j++) {
				GridIndex gi = new GridIndex(i, j);
				if (isBoundary(gi)) {
					boundary.add(gi);
				}
			}
		}
	}

	// private int getClass(GridIndex index){
	// return part[index.row][index.col];
	// }

	private boolean isBoundary(GridIndex index) {
		GridIndex[] neis = getNeighbors(index);
		for (int i = 0; i < neis.length; i++) {
			if (part[index.row][index.col] != part[neis[i].row][neis[i].col]) {
				return true;
			}
		}
		return false;
	}

	private GridIndex[] getNeighbors(GridIndex index) {
		if (ntype == NeighborType.FOUR) {
			return getFourNeighbors(index);
		}
		return getEightNeighbors(index);
	}

	private GridIndex[] getEightNeighbors(GridIndex index) {
		int length;
		if (index.row == 0 || index.row == part.length - 1) {
			if (index.col == 0 || index.col == part[0].length - 1) {
				length = 3;
			} else {
				length = 5;
			}
		} else {
			if (index.col == 0 || index.col == part[0].length - 1) {
				length = 5;
			} else {
				length = 8;
			}
		}
		GridIndex[] gis = new GridIndex[length];
		int currentIndex = 0;
		for (int i = index.row - 1; i <= index.row + 1; i++) {
			for (int j = index.col - 1; j <= index.col + 1; j++) {
				if (i >= 0 && i < part.length && j >= 0 && j < part[0].length) {
					GridIndex gi = new GridIndex(i, j);
					if (index.isNeighbor(gi, NeighborType.EIGHT)) {
						gis[currentIndex] = gi;
						currentIndex++;
					}
				}
			}
		}
		return gis;
	}

	private GridIndex[] getFourNeighbors(GridIndex index) {
		int length;
		if (index.row == 0 || index.row == part.length - 1) {
			if (index.col == 0 || index.col == part[0].length - 1) {
				length = 2;
			} else {
				length = 3;
			}
		} else {
			if (index.col == 0 || index.col == part[0].length - 1) {
				length = 3;
			} else {
				length = 4;
			}
		}
		GridIndex[] gis = new GridIndex[length];
		int currentIndex = 0;
		for (int i = index.row - 1; i <= index.row + 1; i++) {
			for (int j = index.col - 1; j <= index.col + 1; j++) {
				if (i >= 0 && i < part.length && j >= 0 && j < part[0].length) {
					GridIndex gi = new GridIndex(i, j);
					if (index.isNeighbor(gi, NeighborType.FOUR)) {
						gis[currentIndex] = gi;
						currentIndex++;
					}
				}
			}
		}
		return gis;
	}

}

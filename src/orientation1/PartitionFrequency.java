package orientation1;

public class PartitionFrequency implements Comparable<PartitionFrequency> {
	private int[][] grid;
	private int freq;

	/**
	 * A constructor.
	 * 
	 * @param rowNumber
	 *            the number of rows of the grid.
	 * @param colNumber
	 *            the number of columns of the grid.
	 * @param freq
	 *            the frequency of this partition
	 */
	public PartitionFrequency(int rowNumber, int colNumber, int aFreq) {
		grid = new int[rowNumber][colNumber];
		freq = aFreq;
	}

	/**
	 * Another constructor with default frequency 1.
	 * 
	 * @param rowNumber
	 *            the number of rows of the grid.
	 * @param colNumber
	 *            the number of columns of the grid.
	 */
	public PartitionFrequency(int rowNumber, int colNumber) {
		this(rowNumber, colNumber, 1);
	}

	/**
	 * Another constructor for square grid and default frequency 1.
	 * 
	 * @param dim
	 *            the dimension of the square, i.e. number of rows (columns).
	 */
	public PartitionFrequency(int dim) {
		this(dim, dim);
	}

	/**
	 * Another constructor with given grid and frequency.
	 * 
	 * @param aGrid
	 *            a given grid which is a 2-D integer array.
	 * @param aFreq
	 *            the frequency of this partition.
	 */
	public PartitionFrequency(int[][] aGrid, int aFreq) {
		grid = aGrid;
		freq = aFreq;
	}

	/**
	 * Another constructor with given grid and default frequency 1.
	 * 
	 * @param aGrid
	 *            a given grid which is a 2-D integer array.
	 */
	public PartitionFrequency(int[][] aGrid) {
		this(aGrid, 1);
	}

	/**
	 * Merge two identical partitions.
	 * 
	 * @param other another PartitionFrequency object with identical grid.
	 */
	public void merge(PartitionFrequency other) {
		// for use when this.equals(other) is true
		this.freq += other.freq;
	}

	/**
	 * Get the partition which is represented by a 2-D array.
	 * 
	 * @return a 2-D array representing a partition.
	 */
	public int[][] getPartition() {
		return grid;
	}

	/**
	 * Get the frequency of this partition.
	 * 
	 * @return the frequency.
	 */
	public int getFrequency() {
		return freq;
	}

	/**
	 * Implements comapreTo method.
	 * @param other another PartitionFrequency object.
	 * @return an integer indicate the order to the two PartitionFrequency objec.
	 */
	public int compareTo(PartitionFrequency other) {
		return this.freq - other.freq;
	}

	/**
	 * Check whether this Grid object equals another Grid object.
	 * 
	 * @param other
	 *            another Grid object.
	 * @return a boolean value.
	 */
	public boolean equals(PartitionFrequency other) {
		if (other.grid.length != this.grid.length) {
			return false;
		}
		if (other.grid[0].length != this.grid[0].length) {
			return false;
		}
		for (int i = this.grid.length - 1; i >= 0; i--) {
			for (int j = this.grid[0].length - 1; j >= 0; j--) {
				if (this.grid[i][j] != other.grid[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
}

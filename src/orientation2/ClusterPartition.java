package orientation2;

import java.util.ArrayList;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;

import orientation1.NeighborType;


public class ClusterPartition extends Partition {
	/**
	 * A globally shared random number generator. Initialization required.
	 */
	private final RandomDataImpl rng;
	/**
	 * A LLH object.
	 */
	private final LLH llh;
	/**
	 * A ParamPrior object. Initialization required.
	 */
	private final ParamPrior paramPrior;
	/**
	 * A PartitionPrior object. Initialization required.
	 */
	private final PartitionPrior partPrior;
	/**
	 * Observed data. Initialization required. Its content should never be
	 * changed after initialization.
	 */
	private final double[][] data;
	/**
	 * A NeighborType object. Default value NeighborType.Four is used.
	 */
	private final NeighborType ntype;
	/**
	 * A coefficient for determining the rate for points to jump to a singleton.
	 * A default value 1 is used. It's a tuning parameter which controls the
	 * jumping rate of Metropolis-Hasting step. It will be auto adjusted in
	 * simulation if necessary.
	 */
	private final double b1;
	/**
	 * Another coefficient for determining the rate for points to jump to a
	 * singleton. A default value 3 is used. It's another tuning parameter which
	 * controls the jumping rate of Metropolis-Hasting step. It will auto
	 * adjusted in simulation if necessary.
	 */
	private final double b2;
	/**
	 * The weight coefficient for partition complexity part in the prior for
	 * partitions.
	 */
	private final double complexityWeight;
	private final double adjustingWeight;
	/**
	 * Need initialization after data is supplied. No constructor argument
	 * required. It will be synced with the classGrid instance variable
	 */
	private final ArrayList<ArrayList<Point>> listPart;
	/**
	 * Partial Partition Complexity. 
	 * See the definition in PartitionPrior.
	 */
	private double partialPartitionComplexity;
	/**
	 * The difference between the partial complexity of current partition
	 * and the partial complexity of the proposed partition.
	 */
	private double ppComDiff = 0;
	/**
	 * Used to store posterior draws. No need for an argument in the constructor
	 * (similar to instance variable listPart).
	 */
	private final PartitionTable partTable;
	/**
	 * Used by the mcr method. need initialization. However, no need for an
	 * argument in the constructor, just given it any initial value.
	 */
	private Point selectedPoint = new Point(0, 0);
	/**
	 * The selected class.
	 */
	private ArrayList<Point> selectedClass;
	/**
	 * The size of the selected class.
	 */
	private int selectedSize;
	/**
	 * Used by the mcr method, no need to initialize it.
	 */
	private ArrayList<Point> selectedNeighbors;
	/**
	 * Used by the mcr method, no need to initialize it.
	 */
	private Point moveToPoint;
	/**
	 * The class moving to.
	 */
	private ArrayList<Point> moveToClass;
	/**
	 * The size of the class moving to.
	 */
	private int moveToSize;
	/**
	 * A private constructor.
	 * @param builder a Builder object which helps instantiate this class. 
	 */
	private ClusterPartition(Builder builder) {
		super(builder.classGrid);
		rng = builder.rng;
		llh = builder.llh.clone();
		paramPrior = builder.paramPrior.clone();
		partPrior = builder.partPrior.clone();
		data = new double[builder.data.length][builder.data[0].length];
		setData(builder.data);
		ntype = builder.ntype;
		b1 = builder.b1;
		b2 = builder.b2;
		complexityWeight = builder.complexityWeight;
		adjustingWeight = builder.adjustingWeight;
		listPart = TwoDArray.splitArray(getClassGrid());
		partTable = new PartitionTable(new FreqPartition(builder.classGrid));
		partialPartitionComplexity = PartitionPrior.partialPartitionComplexity(listPart);
	}
	/**
	 * A private static builder class which helps instantiate this class.
	 * @author adu
	 *
	 */
	public static class Builder {
		// required parameters
		private final RandomDataImpl rng;
		private final LLH llh;
		private final ParamPrior paramPrior;
		private final PartitionPrior partPrior;
		private final double[][] data;
		private final int[][] classGrid;
		// optional parameters with default values
		private NeighborType ntype = NeighborType.FOUR;
		private double b1 = 1;
		private double b2 = 3;
		private double complexityWeight = 1;
		private double adjustingWeight = 1;

		public Builder(RandomDataImpl aRNG, LLH aLlh, ParamPrior aParamPrior,
				PartitionPrior aPartPrior, double[][] aData, int[][] aClassGrid) {
			rng = aRNG;// use reference because the RNG is shared
			llh = aLlh;
			paramPrior = aParamPrior;
			partPrior = aPartPrior;
			data = aData;
			classGrid = aClassGrid;
		}

		public Builder adjustingWeight(double aAdjustingWeight) {
			adjustingWeight = aAdjustingWeight;
			return this;
		}

		public Builder complexityWeight(double aComplexityWeight) {
			complexityWeight = aComplexityWeight;
			return this;
		}

		public Builder neighborType(NeighborType aNType) {
			ntype = aNType;
			return this;
		}

		public Builder b2(double aB2) {
			b2 = aB2;
			return this;
		}

		public Builder b1(double aB1) {
			b1 = aB1;
			return this;
		}
		
		public ClusterPartition builder(){
			return new ClusterPartition(this);
		}
	}
	
	private void setData(double[][] newData) {
		TwoDArray.copyArray(newData, data);
	}
	/**
	 * Warm up the MCMC for a given number of steps.
	 * @param size the number of steps to warm up the MCMC.
	 * @throws MathException
	 */
	public void warmUp(int size) throws MathException {
		for(int i=0; i<size; i++){
			jump();
		}
		//update partTable
		partTable.getTable().set(0, new FreqPartition(getClassGrid()));
	}
	/**
	 * Run MCMC.
	 * @param size the number of steps to run MCMC.
	 * @throws MathException
	 */
	public void runMCMC(int size) throws MathException{
		boolean isSamePartition;
		for(int i=0; i<size; i++){
			isSamePartition = jump();
			if(isSamePartition){
				partTable.increaseLastFrequency();
			}else{
				partTable.add(new FreqPartition(getClassGrid()));
			}
		}
	}
	/**
	 * Let the partition jumps 1 step forward.
	 * @return a boolean value indicate whether the new partition
	 * is the same as the old one. If they are the same, return true,
	 * and vice versa.
	 * @throws MathException
	 */
	private boolean jump() throws MathException {
		// get a proposal partition using MCR process
		mcr();
		double aRatio = acceptanceRatio();
		if (aRatio >= 1) {
			// accept the proposal
			return updatePartition();
		}
		if (rng.nextBinomial(1, aRatio) == 1) {
			// accept the proposal
			return updatePartition();
		}
		return true;
	}

	/**
	 * Update the partition.
	 * 
	 * @return a boolean value indicate whether the updated partition is the
	 *         same with the original one. If the same then return true, o.w.
	 *         return false.
	 */
	private boolean updatePartition() {
		if(moveToSize==0&&selectedSize==1){
			return true;
		}
		if(selectedPoint.isInSameClass(moveToPoint, getClassGrid())){
			return true;
		}
		//update partial partition complexity
		updatePartialPartitionComplexity();
		//update listPart, it must come before updateClassGrid
		updateList();
		//update the class grid
		updateClassGrid();
		return false;
	}
	/**
	 * Update the ArrayList of ArrayList of points, 
	 * i.e. the object listPart. 
	 * Note that this method must be invoke before method
	 * updateClassGrid, because method updateClassGrid 
	 * mutate the class grid (the 2-D integer array) which 
	 * affects the method updateList.
	 * TODO use the selected class and move to class to update, no need to search again
	 */
	private boolean updateList(){
		if(moveToPoint==null){//move out to a singleton
			if(selectedSize==1){
				return true;
			}
			//get the class id of the selected point
			int selectedID = selectedPoint.getClassID(getClassGrid());
			//find the index of the selected class in listPart
			int selectedClassIndex=-1;
			int size = listPart.size();
			for(int i=0; i<size; i++){
				if(listPart.get(i).get(0).getClassID(getClassGrid())==selectedID){
					selectedClassIndex = i;
					break;
				}
			}
			//find the index of the selected point the in class (ArrayList of points)
			int selectedPointIndex = -1;
			//this local variable selectedClass is not the same as the instance 
			//variable selectedClass
			ArrayList<Point> selectedClass = listPart.get(selectedClassIndex);
			size = selectedClass.size();
			for(int i=0; i<size; i++){
				if(selectedClass.get(i).equals(selectedPoint)){
					selectedPointIndex = i;
					break;
				}
			}
			//move the selected point into a singleton,
			//i.e. add a new ArrayList<Point> into listPart
			//I don't think a singleton class will grow fast, 
			//so I just use the default capacity which is 10
			ArrayList<Point> pts = new ArrayList<Point>();
			//note here you cannot add the instance variable selectedPoint 
			//into it, because it will changed later
			pts.add(selectedClass.get(selectedPointIndex));
			//insert the new ArrayList<Point> after the original class
			//I do this for performance concern
			listPart.add(selectedClassIndex+1,pts);
			//remove the selected point from its original class
			selectedClass.remove(selectedPointIndex);
			return false;
		}
		//get the class id of the selected point and the move-to point
		int selectedID = selectedPoint.getClassID(getClassGrid());
		int moveToID = moveToPoint.getClassID(getClassGrid());
		if(selectedID==moveToID){
			return true;
		}
		//index of the two classes in listPart which 
		//is an ArrayList of ArrayList of points
		int selectedClassIndex = -1;
		int moveToClassIndex = -1;
		//find the indexes for the two classes
		int size = listPart.size();
		int currentID;
		for(int i=0; i<size; i++){
			currentID = listPart.get(i).get(0).getClassID(getClassGrid());
			if(currentID==selectedID){
				selectedClassIndex = i;
				if(moveToClassIndex>=0){
					break;
				}
			}else if(currentID==moveToID){
				moveToClassIndex = i;
				if(selectedClassIndex>=0){
					break;
				}
			}
		}
		//find the index of the selected point in its class (ArrayList of points)
		int selectedPointIndex = -1;
		//this local variable selectedClass is not the same as the instance 
		//variable selectedClass
		ArrayList<Point> selectedClass = listPart.get(selectedClassIndex);
		size = selectedClass.size();
		for(int i=0; i<size; i++){
			if(selectedClass.get(i).equals(selectedPoint)){
				selectedPointIndex = i;
				break;
			}
		}
		//move the selected point to the move-to class
		//note that you cannot add the instance variable selectedPoint 
		//into the move-to class, because ...
		moveToClass.add(selectedClass.get(selectedPointIndex));
		//remove the selected point from the selected class
		selectedClass.remove(selectedPointIndex);
		if(selectedClass.size()==0){
			//remove the empty ArrayList<Point>
			listPart.remove(selectedClassIndex);
		}
		return false;
	}
	/**
	 * Update the partial partition complexity, 
	 * which is a robust way.
	 */
	private boolean updatePartialPartitionComplexity(){
		if(moveToSize==0&&selectedSize==1){
			return true;
		}
		if(selectedPoint.isInSameClass(moveToPoint, getClassGrid())){
			return true;
		}
		partialPartitionComplexity -= ppComDiff;
		return false;
	}
	/**
	 * Get the ID of the move-to class.
	 * If the move-to class is empty, 
	 * then the class ID of the selected point is return (because
	 * in this case it's the future ID of the move-to class).
	 * @return the ID of the move-to class.
	 */
	private int getMoveToID(){
		if(moveToPoint==null){
			return selectedPoint.getUnivariateIndex(getColumnNumber());
		}
		return moveToPoint.getClassID(getClassGrid());
	}
	/**
	 * Update the class ID of the selected class.
	 */
	private void updateSelectedClassID(){
		//make sure the selected point is removed from the selected class
		if(selectedClass.size()==selectedSize){
			//remove the selected point from the selected class
			//since it's the last element of the ArrayList, 
			//we can just remove the last element of the ArrayList.
			selectedClass.remove(selectedSize-1);
		}
		if(selectedClass.size()==0){
			return;
		}
		//get the new class ID
		//since the elements in selectedClass are sorted according 
		//to their univariate index, 
		//the new ID is the univariate index of the first element
		int newID = selectedClass.get(0).getUnivariateIndex(getColumnNumber());
		setClassGrid(selectedClass,newID);
	}
	/**
	 * Update the class ID of the move-to class.
	 * @param newID a new class ID.
	 */
	private void updateMoveToClassID(int newID){
		//make sure the selected point is added into moveToClass
		if(moveToClass.size()==moveToSize){
			moveToClass.add(selectedPoint);
		}
		if(moveToClass.size()==1){
			return;
		}
		setClassGrid(moveToClass,newID);
	}
	/**
	 * Update the class grid, i.e. update the 2-D integer array.
	 */
	private boolean updateClassGrid(){
		if(moveToSize==0&&selectedSize==1){
			return true;
		}
		if(selectedPoint.isInSameClass(moveToPoint, getClassGrid())){
			return true;
		}
		int selectedID = selectedPoint.getClassID(getClassGrid());
		int moveToID = getMoveToID();
		setClassGrid(selectedPoint,moveToID);
		int selectedUnivariateIndex = selectedPoint.getUnivariateIndex(getColumnNumber());
		if(selectedID==selectedUnivariateIndex){
			//update the left points in the selected class
			updateSelectedClassID();
		}
		if(selectedUnivariateIndex<moveToID){
			//update points in moveToClass including the newly moved in point
			updateMoveToClassID(selectedUnivariateIndex);
		}
		return false;
	}
	/**
	 * The acceptance ratio.
	 * 
	 * @return the acceptance ratio.
	 */
	private double acceptanceRatio() {
		double logRatio = logAcceptanceRatio();
		if (logRatio >= 0) {
			return 2;// as long as it's greater than or equal to 1
		}
		return Math.exp(logRatio);
	}

	/**
	 * The logarithm of the acceptance ratio. It's for numerical consideration
	 * not to calculate the acceptance ratio directly.
	 * 
	 * @return the logarithm of the acceptance ratio.
	 */
	private double logAcceptanceRatio() {
		double logRatio = logMCRProbDiff();
		//add the log partition difference, it must come before
		//the log likelihood difference, because the logLLHDiff method
		//destroys the instance variables selectedClass and moveToClass.
		logRatio += logPartitionPriorDiff();
		logRatio += logLLHDiff();
		return logRatio;
	}

	/**
	 * Generate a proposal using Modified Chinese Restaurant Process.
	 */
	private void mcr() {
		// randomly select a point
		int row = rng.nextInt(0, getRowNumber() - 1);
		int column = rng.nextInt(0, getColumnNumber()-1);
		selectedPoint.set(row, column);
		// find its neighbors
		selectedNeighbors = selectedPoint.getNeighbors(getRowNumber(),
				getColumnNumber(), ntype);
		//find the class where it is
		selectedClass = selectedPoint.getSameClass(listPart, getClassGrid());
		selectedSize = selectedClass.size();
		// check whether to move it as a singleton or not
		if (rng.nextUniform(0, 1) <= selectedPoint.getAlpha(b1, b2,
				getRowNumber(), getColumnNumber(), ntype)) {
			// move this selected point to a singleton
			moveToPoint = null;
			moveToClass = new ArrayList<Point>(1);//at most 1 element
			moveToSize = 0;//will not be used
			return;
		}
		// randomly choose a neighbor
		int index = rng.nextInt(0, selectedNeighbors.size() - 1);
		moveToPoint = selectedNeighbors.get(index);
		moveToClass = moveToPoint.getSameClass(listPart, getClassGrid());
		moveToSize = moveToClass.size();
	}

	/**
	 * The difference between the logarithms of the MCR probabilities, which is
	 * simply the logarithm of the ratio of MCR.
	 * 
	 * @return the difference between the logarithms of the MCR probabilities.
	 */
	private double logMCRProbDiff() {
		return Math.log(mcrProbQ());
	}

	/**
	 * The ratio of MCR probabilities of the current one given proposed to the
	 * proposed given the current one. This ratio doesn't have numerical
	 * problems and is calculated directly.
	 * 
	 * @return the ratio of MCR probabilities.
	 */
	private double mcrProbQ() {
		if (moveToSize == 0) {// moved to a singleton
			if (selectedSize == 1) {// from a singleton to a singleton
				return 1;
			}
			if (selectedSize == 2) {// from a set with 2 points to a singleton,
				// if the other point in the set is selected and moved out as a
				// singleton, the same partition results.
				double r1 = (double) selectedPoint.countSameClass(
						selectedNeighbors, getClassGrid())
						/ selectedNeighbors.length;
				//get the other point in the same class
				//note that selectePoint is the last element, so 
				//the first element is the other point
				Point otherPoint = selectedClass.get(0).clone();
				Point[] otherNeighbors = otherPoint.getNeighbors(getRowNumber(),
						getColumnNumber(), ntype);
				double r2 = (double) otherPoint.countSameClass(otherNeighbors,
						getClassGrid()) / otherNeighbors.length;
				double alpha1 = selectedPoint.getAlpha(b1, b2, getRowNumber(),
						getColumnNumber(), ntype);
				double alpha2 = otherPoint.getAlpha(b1, b2, getRowNumber(),
						getColumnNumber(), ntype);
				return ((1 - alpha1) * r1 + (1 - alpha2) * r2)
						/ (alpha1 + alpha2);
			}
			double alpha = selectedPoint.getAlpha(b1, b2, getRowNumber(),
					getColumnNumber(), ntype);
			return ((1 - alpha) * selectedPoint.countSameClass(
					selectedNeighbors, getClassGrid()))
					/ (alpha * selectedNeighbors.length);
		}// moved to the set in which moveToPoint is
			// check whether they are in the same class
		if (selectedPoint.isInSameClass(moveToPoint, getClassGrid())){
			return 1;
		}
		if (selectedSize==1) {// the selected point is
														// a singleton
			if (moveToSize==1) {// moveToPoint is a
															// singleton
				double alpha1 = selectedPoint.getAlpha(b1, b2, getRowNumber(),
						getColumnNumber(), ntype);
				double alpha2 = moveToPoint.getAlpha(b1, b2, getRowNumber(),
						getColumnNumber(), ntype);
				// count how many neighbors of selectedPoint have the same class
				// as movetoPoint
				double r1 = (double) moveToPoint.countSameClass(
						selectedNeighbors, getClassGrid())
						/ selectedNeighbors.length;
				// count how many neighbors of moveToPoint have the same class
				// as selectedPoint
				Point[] moveToNeighbors = moveToPoint.getNeighbors(
						getRowNumber(), getColumnNumber(), ntype);
				double r2 = (double) selectedPoint.countSameClass(
						moveToNeighbors, getClassGrid())
						/ moveToNeighbors.length;
				return (alpha1 + alpha2)
						/ ((1 - alpha1) * r1 + (1 - alpha2) * r2);
			}// moveToPoint is not a singleton
			double alpha = selectedPoint.getAlpha(b1, b2, getRowNumber(),
					getColumnNumber(), ntype);
			return alpha
					* selectedNeighbors.length
					/ ((1 - alpha) * moveToPoint.countSameClass(
							selectedNeighbors, getClassGrid()));
		}// the selected point is not a singleton
		return (double) selectedPoint.countSameClass(selectedNeighbors,
				getClassGrid())
				/ moveToPoint.countSameClass(selectedNeighbors, getClassGrid());
	}

	/**
	 * The difference between logarithms of the partition priors for the
	 * proposed partition and the current one (proposed - current).
	 * 
	 * @return the difference between logarithms of the partition priors.
	 */
	private double logPartitionPriorDiff() {
		if (moveToSize == 0) {// move to a singleton
			if (selectedSize == 1) {// selectedPoint is a singleton
				ppComDiff = 0;
				return 0;
			}// selectedPoint is not a singleton
			if (selectedSize == 2) {// there are 2 points in the class of the selected
							// point
				double setDiff = QuadraticPartitionPrior.setComplexity(selectedClass);
				ppComDiff = (setDiff + partialPartitionComplexity)
						/ (listPart.size() + 1);
				double comDiff = ppComDiff
						+ partPrior.partitionSizePenalty(listPart.size())
						- partPrior.partitionSizePenalty(listPart.size() + 1);
				double adjDiff = partPrior.adjusting(listPart.size())
						- partPrior.adjusting(listPart.size() + 1);
				return complexityWeight * comDiff + adjustingWeight * adjDiff;
			}// there are multiple points in the class of the selected point
			double setDiff = (selectedPoint.distance(selectedClass)
					/ (selectedSize - 1) - QuadraticPartitionPrior
						.setComplexity(selectedClass))
					/ (selectedSize - 2)
					* 2d;
			ppComDiff = (setDiff + partialPartitionComplexity)
					/ (listPart.size() + 1); 
			double comDiff = ppComDiff
					+ partPrior.partitionSizePenalty(listPart.size())
					- partPrior.partitionSizePenalty(listPart.size() + 1);
			double adjDiff = partPrior.adjusting(listPart.size())
					- partPrior.adjusting(listPart.size() + 1);
			return complexityWeight * comDiff + adjustingWeight * adjDiff;
		}// move to an existing set
		if(selectedPoint.isInSameClass(moveToPoint, getClassGrid())){
			ppComDiff = 0;
			return 0;
		}		
		if (selectedSize == 1) {// the selected point is a singleton
			double setDiff = (selectedPoint.distance(moveToClass) / moveToSize 
					- QuadraticPartitionPrior.setComplexity(moveToClass)) / (moveToSize + 1) * 2d;
			ppComDiff = -(setDiff + partialPartitionComplexity)
					/ (listPart.size() - 1); 
			double comDiff = ppComDiff
					+ partPrior.partitionSizePenalty(listPart.size())
					- partPrior.partitionSizePenalty(listPart.size() - 1);
			double adjDiff = partPrior.adjusting(listPart.size())
					- partPrior.adjusting(listPart.size() - 1);
			return complexityWeight * comDiff + adjustingWeight * adjDiff;
		}// the selected point is not a singleton
		// set difference for selected class and its variation
		double setDiff1 = QuadraticPartitionPrior.setComplexity(selectedClass);
		if (selectedSize > 2) {
			setDiff1 = (selectedPoint.distance(selectedClass)
					/ (selectedSize - 1) - QuadraticPartitionPrior
						.setComplexity(selectedClass))
					/ (selectedSize - 2)
					* 2d;
		}	
		double setDiff2 = (QuadraticPartitionPrior.setComplexity(moveToClass) - selectedPoint
				.distance(moveToClass) / moveToSize)
				/ (moveToSize + 1) * 2d;
		ppComDiff = (setDiff1 + setDiff2) / listPart.size(); 
		double comDiff = ppComDiff; // no penalty or adjusting change
		return comDiff;
	}

	/**
	 * Get the number of columns of the grid.
	 * 
	 * @return the number of columns of the grid.
	 */
	private int getColumnNumber() {
		return data[0].length;
	}

	/**
	 * Get the number of rows of the grid.
	 * 
	 * @return the number of row of the grid.
	 */
	private int getRowNumber() {
		return data.length;
	}

	/**
	 * The difference between the logarithms of the likelihoods for the proposed
	 * partition and the current one.
	 * 
	 * @return the difference between the logarithm of the likelihoods.
	 */
	private double logLLHDiff() {
		if (moveToSize == 0) {
			// moved out to a singleton, 3 parameters needed
			if(selectedSize==1){
				return 0;
			}
			// generate parameter for the original set S1
			double theta1 = paramPrior.nextPost(selectedClass, data);
			//generate parameter for the singleton set P0
			double theta3 = paramPrior.next();
			//generate parameter for the left set S1/P0
			double theta4 = paramPrior.next();
			// calculate the log likelihood
			double s1 = llh.llhSum(selectedClass, data, theta1);
			selectedClass.remove(selectedSize-1);
			double s3 = llh.llhSum(selectedClass, data, theta3);
			double s4 = llh.llh(
					data[selectedPoint.row][selectedPoint.column], theta4);
			return s3 + s4 - s1;
		} 
		//check whether the two points are in the same class
		if(selectedPoint.isInSameClass(moveToPoint, getClassGrid())){
			return 0;
		}
		if(selectedSize==1){
			//singleton moved to another set, 3 parameters needed
			//generate parameter for the singleton
			double theta1 = paramPrior.nextPost(selectedClass, data);
			double theta2 = paramPrior.nextPost(moveToClass, data);
			//generate parameter for the resulting set
			double theta4 = paramPrior.next();
			double s1 = llh.llhSum(selectedClass, data, theta1);
			double s2 = llh.llhSum(moveToClass, data, theta2);
			moveToClass.add(selectedPoint);
			double s4 = llh.llhSum(moveToClass, data, theta4);
			return s4-s1-s2;
		}
		// moved out to another class, 4 parameters needed
		double theta1 = paramPrior.nextPost(selectedClass, data);
		double theta2 = paramPrior.nextPost(moveToClass, data);
		double theta3 = paramPrior.next();
		double theta4 = paramPrior.next();
		double s1 = llh.llhSum(selectedClass, data, theta1);
		double s2 = llh.llhSum(moveToClass, data, theta2);
		selectedClass.remove(selectedSize-1);
		double s3 = llh.llhSum(selectedClass, data, theta3);
		moveToClass.add(selectedPoint);
		double s4 = llh.llhSum(moveToClass, data, theta4);
		return s3 + s4 - s1 - s2;
	}
}

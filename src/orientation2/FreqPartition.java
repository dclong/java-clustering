package orientation2;

public class FreqPartition extends Partition implements Comparable<FreqPartition> {
	private int freq;
	/**
	 * A constructor.
	 * @param aClassGrid a 2-D integer array, which 
	 * specifies a partition.
	 * @param aFreq a positive integer. 
	 */
	public FreqPartition(int[][] aClassGrid, int aFreq){
		super(aClassGrid);
		freq = aFreq;
	}
	/**
	 * Another constructor.
	 * @param aClassGrid a 2-D integer array, which specifies 
	 * a partition.
	 */
	public FreqPartition(int[][] aClassGrid){
		this(aClassGrid,1);
	}
	/**
	 * Another constructor, which construct a new FreqPartition 
	 * which has identical states with the given one. 
	 * @param other a FreqPartition object.
	 */
	public FreqPartition(FreqPartition other){
		this(other.getClassGrid(),other.freq);
	}
	/**
	 * Get the frequency of this FreqPartition.
	 * @return
	 */
	public int getFrequency(){
		return freq;
	}
	/**
	 * Increase the frequency of this FreqPartition.
	 * @param increasement an integer value.
	 */
	public void increaseFrequency(int increasement){
		freq += increasement;
	}
	/**
	 * Merge two same partitions (i.e. partitions with a same class grid).
	 * The resulting partition have a frequency which is sum of the original two.  
	 * @param other a FreqPartition object.
	 */
	public void merge(FreqPartition other){
		freq += other.freq;
	}
	/**
	 * Implements comapreTo method. 
	 * The order comparison of two FreqPartition objects are based on 
	 * their frequency not the class grid they hold. 
	 * @param other another PartitionFrequency object.
	 * @return an integer indicate the order to the two PartitionFrequency objec.
	 */
	public int compareTo(FreqPartition other) {
		return this.freq - other.freq;
	}
	/**
	 * A override version of equals method which is compatible with 
	 * the implemented method compareTo, i.e. this.compareTo(other)==0 
	 * is equivalent to this.equals(other). 
	 * To compare whether two FreqPartition objects have the same partition,
	 * you should use method isSameWith.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FreqPartition other = (FreqPartition) obj;
		return this.freq==other.freq;
	}	
	
	/**
	 * Check whether the two partitions are the same, 
	 * i.e. whether the two 2-D arrays are the same.
	 * 
	 * @param other
	 *            another Grid object.
	 * @return a boolean value.
	 */
	public boolean isSameWith(FreqPartition other) {
		if(this==other){
			return true;
		}
		if(other==null){
			return false;
		}
		int[][] thisGrid = getClassGrid();
		int[][] otherGrid = other.getClassGrid();
		return TwoDArray.equals(thisGrid, otherGrid);
	}
}

package orientation2;

import java.util.ArrayList;

public abstract class LLH {
	/**
	 * An abstract clone method, which returns a new 
	 * object with identical states with this object.
	 */
	public abstract LLH clone();
	/**
	 * An abstract llh method, which calculates 
	 * the log likelihood.
	 * @param x a real value.
	 * @param theta a real value.
	 * @return the log likelihood.
	 */
	abstract double llh(double x, double theta);
	/**
	 * The logarithm of the joint likelihood,
	 * i.e. the sum of the log-likelihoods. 
	 * Note that this method works for an empty set. 
	 * @param set an ArrayList of points.
	 * @param data a 2-D array containing data.
	 * @param theta a real value.
	 * @return the logarithm of the joint likelihood.
	 */
	public double llhSum(ArrayList<Point> set,double[][] data,double theta){
		int size = set.size();
		double sum = 0;
		Point pt;
		for(int i=0; i<size; i++){
			pt = set.get(i);
			sum += llh(data[pt.row][pt.column],theta);
		}
		return sum;
	}
}

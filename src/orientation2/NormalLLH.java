package orientation2;
/**
 * This is the likelihood and log likelihood function for 
 * the N(theta,1) distribution. Because it's for MCMC use, 
 * only a proportional value for the likelihood is calculated.
 * @author adu
 *
 */
public class NormalLLH extends LLH {
//	/**
//	 * The density of the N(theta,1) distribution.
//	 * @param x a real value at which the density is evaluated.
//	 * @param theta the parameter of the N(theta,1) distribution.
//	 * @return the density value at x.
//	 */
//	public static double pdf(double x, double theta){
//		double diff = x-theta;
//		return Math.exp(-0.5*diff*diff);
//	}
	public NormalLLH(){
		//do nothing
	}
	/**
	 * Implements the abstract clone method.
	 */
	@Override
	public NormalLLH clone(){
		return new NormalLLH();
	}
	/**
	 * Implements the abstract method llh.
	 */
	public double llh(double x, double theta){
		double diff = x-theta;
		return -0.5*diff*diff;
	}
}

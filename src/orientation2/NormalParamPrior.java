package orientation2;

import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;

/**
 * This class specifies a normal prior for the parameters, and 
 * generate random observations from parameters given a partition. 
 * @author adu
 *
 */
public class NormalParamPrior extends ParamPrior {
	private double mean;
	private double  sd;
	private double var;
	/**
	 * A constructor.
	 * @param aRNG a random number generator.
	 * @param aMean a real value.
	 * @param aSD a positive real value.
	 */
	public NormalParamPrior(RandomDataImpl aRNG,double aMean,double aSD){
		super(aRNG);
		setMean(aMean);
		setSD(aSD);
	}
	/**
	 * Implements the abstract clone method.
	 */
	@Override
	public NormalParamPrior clone() {
		return new NormalParamPrior(rng,mean,sd);
	}
	/**
	 * Implements the abstract method nextPost.
	 */
	public double nextPost(ArrayList<Point> set,double[][] data){
		double denominator = 1 + var*set.size();
		double postMean = (setSum(set,data)*var+mean)/denominator;
		double postSD = sd/Math.sqrt(denominator);
		return rng.nextGaussian(postMean, postSD);
	}
	
	private double setSum(ArrayList<Point> set, double[][] data){
		double sum = 0;
		Point pt;
		int size = set.size();
		for(int i=0; i<size; i++){
			pt = set.get(i);
			sum += data[pt.row][pt.column];
		}
		return sum;
	}
	/**
	 * Implements the abstract next method.
	 */
	public double next(){
		return rng.nextGaussian(mean, sd);
	}
	
//	public double pdf(double x){
//		double temp = (x - mean)/sd;
//		return Math.exp(-0.5*temp*temp)/(Math.sqrt(2*Math.PI)*sd);
//	}
	
	public void set(double aMean, double aSD){
		setMean(aMean);
		setSD(aSD);
	}
	
	public void setSD(double aSD){
		sd = aSD;
		var = sd*sd;
	}
	
	public void setMean(double aMean){
		mean = aMean;
	}
}

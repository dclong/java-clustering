package orientation2;

import java.util.ArrayList;

public class Partition {
	private int[][] classGrid;
	/**
	 * A constructor.
	 * @param row
	 * @param column
	 */
	public Partition(int row, int column){
		classGrid = new int[row][column];
	}
	/**
	 * Another constructor.
	 * @param aClassGrid
	 */
	public Partition(int[][] aClassGrid){
		this(aClassGrid.length,aClassGrid[0].length);
		setClassGrid(aClassGrid);
	}
	/**
	 * Set the class grid to a given 2-D integer array.
	 * @param aClassGrid
	 */
	private void setClassGrid(int[][] aClassGrid){
		TwoDArray.copyArray(aClassGrid, classGrid);
	}
	/**
	 * Get the class grid.
	 * @return the class grid.
	 */
	public int[][] getClassGrid(){
		return classGrid;
	}
	/**
	 * Set the selected element to the specified class.
	 * @param rowIndex the row index of the selected element.
	 * @param columnIndex the column index of the selected element.
	 * @param classID a new class ID.
	 */
	public void setClassGrid(int rowIndex, int columnIndex, int classID){
		classGrid[rowIndex][columnIndex] = classID;
	}
	/**
	 * Set the selected element to the specified class.
	 * @param point a point.
	 * @param classID a new class ID.
	 */
	public void setClassGrid(Point point, int classID){
		setClassGrid(point.row,point.column,classID);
	}
	/**
	 * Set a list of elements to be the specified class.
	 * @param pts an ArrayList of points.
	 * @param classID a new class ID.
	 */
	public void setClassGrid(ArrayList<Point> pts, int classID){
		int size = pts.size();
		for(int i=0; i<size; i++){
			setClassGrid(pts.get(i),classID);
		}
	}
}

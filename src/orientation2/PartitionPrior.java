package orientation2;

import java.util.ArrayList;

public abstract class PartitionPrior {
	/**
	 * An abstract clone method, which return as new
	 * object which has identical states with this object. 
	 */
	public abstract PartitionPrior clone();
	/**
	 * An abstract adjusting function.
	 * @param x a real value.
	 * @return a real value.
	 */
	public abstract double adjusting(double x);
	/**
	 * Define a size penalty function for partitions.
	 * This penalty together with the partial complexity of partition 
	 * constitute the (complete) partition complexity (see the definition 
	 * for complete partition complexity. The concept of size penalty 
	 * here is adopted to make it easier to choose an adjusting function (see the
	 * definition for adjusting function), which can be some convex function
	 * with minimized at a user-preferred size. 
	 * You can overwrite this method if needed.
	 * @param size the size of a partition.
	 * @return the penalty at size.
	 */
	public double partitionSizePenalty(int size){
		return size/6d;
	}
	/**
	 * Calculate the partial partition complexity for 
	 * a given class grid (partition).
	 * @param classGrid a 2-D array recording classes of points. 
	 * @return the partial partition complexity.
	 */
	public static double partialPartitionComplexity(int[][] classGrid){
		return partialPartitionComplexity(TwoDArray.splitArray(classGrid));
	}
	
	/**
	 * Calculate a partial complexity for a given partition.
	 * @param aPart an ArrayList of ArrayList of points, which 
	 * represent a partition.
	 * @return the partial complexity of the partition.
	 */
	public static double partialPartitionComplexity(ArrayList<ArrayList<Point>> aPart){
		//get number of subsets
		int size = aPart.size();
		if(size<=0){
			return 0;
		}
		double complexity = 0;
		for(int i=0; i<size; i++){
			complexity += setComplexity(aPart.get(i));
		}
		return complexity/size;
	}
	/**
	 * Define a complexity for a set, which is the 
	 * average distances between points in the set.
	 * @param aSet an ArrayList of points.
	 * @return the distance between the point and the set.
	 */
	public static double setComplexity(ArrayList<Point> aSet){
		int size = aSet.size();
		if(size<=1){
			return 0;
		}
		double sumDist = 0;
		for(int i=0; i<size-1; i++){
			for(int j=i+1; j<size; j++){
				sumDist += aSet.get(i).distance(aSet.get(j));
			}
		}
		return sumDist/(0.5*size*(size-1));
	}
	
//	/**
//	 * Define a (complete) complexity for a partition, 
//	 * which is the average of subsets' complexity (i.e. the partial 
//	 * partition complexity) plus n/6, where n is the number of subsets.
//	 * Note that the n/6 is introduced by myself to penalize a partition
//	 * of being having too many subsets. This penalty term is introduced 
//	 * by an approximation to make the partition complexity convex. It should
//	 * work for a nearly square grid. For other kind of grid, you probably
//	 * need a different penalty term.
//	 * @param aPart an ArrayList of ArrayList of points.
//	 * @return the complexity of the partition.
//	 */
//	private double partitionComplexity(ArrayList<ArrayList<Point>> aPart){
//		return PartitionPrior.partialPartitionComplexity(aPart) + partitionSizePenalty(aPart.size());
//	}
//	/**
//	 * Calculate the (complete) partition complexity for 
//	 * a given class grid (partition).
//	 * @param classGrid a 2-D array recording classes of points.
//	 * @return the (complete) partition complexity.
//	 */
//	public double partitionComplexity(int[][] classGrid){
//		return partitionComplexity(TwoDArray.splitArray(classGrid));
//	}
}

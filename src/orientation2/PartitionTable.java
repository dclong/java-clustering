package orientation2;

import java.util.ArrayList;
/**
 * A Partition table, which keeps track of frequency of partitions.
 * To speed up the program, the partitions except the newest one
 * are sorted according to their frequency in the table. 
 * The partition with a higher frequency has a bigger index in the table. 
 * The newest partition is always at the last place in the table, 
 * i.e. its index is size-1.
 * @author adu
 *
 */
public class PartitionTable {
	/**
	 * A sorted ArrayList of FreqPartition with the first element has the 
	 * highest frequency. It's used to tabulate the posterior draws of partitions. 
	 */
	private ArrayList<FreqPartition> fpList;
	/**
	 * The default constructor.
	 */
	public PartitionTable(){
		this(1000);
	}
	
	public PartitionTable(int initialCapacity){
		fpList = new ArrayList<FreqPartition>(initialCapacity);
	}
	/**
	 * Another constructor.
	 * @param fPart
	 */
	public PartitionTable(FreqPartition fPart){
		this();
		fpList.add(fPart);
	}
	/**
	 * Increase the frequency of the first partition in the ArrayList
	 * by 1. This is useful when the proposed partition is 
	 * rejected. 
	 * @param increasement an integer.
	 */
	public void increaseLastFrequency(){
		fpList.get(fpList.size()-1).increaseFrequency(1);
	}
	/**
	 * Add a new FreqPartition object into the ArrayList. 
	 * Then search from end to beginning to see whether there's 
	 * a FreqPartition object which is the same as the newest one.
	 * If so, then the newest one is updated, and the old one is 
	 * removed. And then the ArrayList is sorted for the last but 1
	 * element (other elements except the last one are already sorted). 
	 * @param fPart a FreqPartition object.
	 */
	public void add(FreqPartition fPart){
		//get the last but 2 index in the resulting ArrayList
		int index = fpList.size()-2;
		//add fPart to the end of the list
		fpList.add(fPart);
		//search to for a same partition
		FreqPartition currentPartition;
		for(int i=index; i>=0; i--){
			currentPartition = fpList.get(i);
			if(fPart.isSameWith(currentPartition)){
				fPart.merge(currentPartition);
				fpList.remove(i);
				sort();
				return;
			}
		}
	}
//	/**
//	 * Add a new element into the ArrayList,
//	 * and then sort it. 
//	 * @param fPart a FreqPartition object. 
//	 */
//	public void add(FreqPartition fPart){
//		//check whether there's any FreqPartition in fpList which is the same as fPart
//		int size = fpList.size();
//		for(int i=0; i<size; i++){
//			FreqPartition currentPartition = fpList.get(i);
//			if(currentPartition.isSameWith(fPart)){
//				//merge the two partitions
//				currentPartition.merge(fPart);
//				sort(i);
//				return;
//			}
//		}
//		//add fPart to the end of fpList
//		fpList.add(fPart);
//		sort(size+1);
//	}
//	/**
//	 * A sort method. Since each time at most one element change, 
//	 * there's no need to use the usual sorting method. 
//	 * Work on the changed/inserted element directly can speed up
//	 * the program. Also partitions have the same frequency 
//	 * are sorted by the order they come out. The newest one goes
//	 * first. 
//	 * @param index the index of the partition that has been changed.
//	 * Only partitions before it (inclusive) need to be sorted.
//	 */
//	private void sort(int index){
//		//make a copy of the changed partition
//		FreqPartition changedPartition = new FreqPartition(fpList.get(index));
//		for(int i=index; i>0; i--){
//			FreqPartition currentPartition = fpList.get(i-1);
//			if(changedPartition.compareTo(currentPartition)>=0){
//				fpList.set(i, currentPartition);
//			}else{
//				fpList.set(i, changedPartition);
//				return;
//			}
//		}
//	}
	/**
	 * Sort the last but 1 element in the ArrayList.
	 */
	private void sort(){
		//get the index of the last but 1 element
		int index = fpList.size()-2;
		if(index<=0){
			return;
		}
		//make a copy of the last but 1 partition
		FreqPartition lastButOnePartition = new FreqPartition(fpList.get(index));
		FreqPartition currentPartition;
		for(int i=index; i>0; i--){
			currentPartition = fpList.get(i-1);
			if(lastButOnePartition.compareTo(currentPartition)<0){
				fpList.set(i, currentPartition);
			}else{
				fpList.set(i,lastButOnePartition);
				return;
			}
		}
	}
	/**
	 * Get the partition table, i.e. the ArrayList of FreqPartition objects. 
	 * @return the partition table. 
	 */
	public ArrayList<FreqPartition> getTable(){
		return fpList;
	}
}

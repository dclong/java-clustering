package orientation2;

import java.util.ArrayList;

import orientation1.NeighborType;


public class Point {
	protected int row;
	protected int column;

	public Point(int aRow, int aColumn) {
		row = aRow;
		column = aColumn;
	}
	/**
	 * The default constructor.
	 */
	public Point(){
		this(0,0);
	}
	/**
	 * Override the clone method.
	 */
	@Override
	public Point clone() {
		return new Point(row, column);
	}

	/**
	 * Override the toString method.
	 */
	@Override
	public String toString() {
		return "Point [row=" + row + ", column=" + column + "]";
	}

	/**
	 * Get the 1-D index of an element in a regular 2-D array.
	 * 
	 * @param columnNumber
	 *            the number of columns in the regular 2-D array.
	 * @return
	 */
	public int getUnivariateIndex(int columnNumber) {
		return TwoDArray.getUnivariateIndex(row, column, columnNumber);
	}

	/**
	 * Calculate alpha which is the probability for this point to jump to a
	 * singleton in the Modified Chinese Restaurant Process. It's calculate as
	 * 1/(b1+b2*n), where n is the number of its neighbors.
	 * 
	 * @param b1
	 *            a coefficient for determining alpha.
	 * @param b2
	 *            another coefficient for determining alpha.
	 * @param rowNumber
	 *            the number of rows in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @return the probability for this point to jump to a singleton in the
	 *         Modified Chinese Restaurant Process.
	 */
	public double getAlpha(double b1, double b2, int rowNumber,
			int columnNumber, NeighborType ntype) {
		return 1 / (b1 + b2 * countNeighbors(rowNumber, columnNumber, ntype));
	}

	/**
	 * Define a distance between a point and a set of points, which is simply
	 * the sum of the distances between the point and points in the set.
	 * 
	 * @param aSet
	 * @return
	 */
	public double distance(ArrayList<Point> aSet) {
		int size = aSet.size();
		double sumDist = 0;
		for (int i = 0; i < size; i++) {
			sumDist += distance(aSet.get(i));
		}
		return sumDist;
	}

	/**
	 * Define a distance between two 2-D points, which is the square of the
	 * usual Euclidean distance.
	 * 
	 * @param other
	 *            another point.
	 * @return the distance between the two points.
	 */
	public double distance(Point other) {
		double rowDiff = row - other.row;
		double columnDiff = column - other.column;
		return rowDiff * rowDiff + columnDiff * columnDiff;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	/**
	 * Get the complete set of points which have the same class as this point.
	 * This method uses binary search to find the right element in an ArrayList
	 * of ArrayList of points, so it requires that elements in the ArrayList of
	 * ArrayList of points to be sorted by class ID. To make further programming
	 * easier and faster, the return result is just a reference of the
	 * corresponding element in the ArrayList of ArrayList of points, in which
	 * points are sorted according to their univariate index. To find a point in
	 * the returned list quickly, you can use binary search.
	 * 
	 * @param listPart
	 *            an ArrayList of ArrayList of points.
	 * @param classGrid
	 *            a 2-D integer array recording class IDs.
	 * 
	 * @return an ArrayList of points or null if noting found.
	 */
	public ArrayList<Point> getSameClass(ArrayList<ArrayList<Point>> listPart,
			int[][] classGrid) {
		// get the class id of this point
		int classID = getClassID(classGrid);
		// initial boundary
		int size = listPart.size();
		int low = 0;
		int high = size - 1;
		// locate target
		int middle = (low + high) / 2;
		ArrayList<Point> target = listPart.get(middle);
		int currentClassID = target.get(0).getClassID(classGrid);
		while (currentClassID != classID) {
			// check boundary
			if (high == low) {// nothing found
				return null;
			}
			// check which direction to go
			if (currentClassID > classID) {
				high = middle - 1;
				middle = (low + high) / 2;
				target = listPart.get(middle);
				currentClassID = target.get(0).getClassID(classGrid);
			} else {
				low = middle + 1;
				middle = (low + high) / 2;
				target = listPart.get(middle);
				currentClassID = target.get(0).getClassID(classGrid);
			}
		}
		// target found
		return target;
	}

	/**
	 * Get a point (not this one) that have the same class as this point. The
	 * found point is the first that satisfies the conditions.
	 * 
	 * @param aArrayList
	 *            an ArrayList of points.
	 * @return a point with the same class as this point.
	 */
	public Point getOtherPoint(ArrayList<Point> aArrayList) {
		int size = aArrayList.size();
		Point other;
		for (int i = 0; i < size; i++) {
			other = aArrayList.get(i);
			if (!equals(other)) {
				return other;
			}
		}
		return null;
	}

	// /**
	// * Get a point (not this one) that have the same class as this point. This
	// * method tries to find such a point quickly, but the one found is not
	// * necessarily the first or the last one. It's probably a point with the
	// same
	// * class around this point.
	// *
	// * @param classGrid
	// * a 2-D array recording the classes of points.
	// * @return a point with the same class as this point.
	// */
	// public Point getOtherPoint(int[][] classGrid) {
	// // first searching around this point
	// for (int i = row - 1; i <= row + 1; i++) {
	// for (int j = column - 1; j <= column + 1; j++) {
	// if (i >= 0 && i < classGrid.length && j >= 0 && j < classGrid[0].length
	// && !(i == row && j == column)) {
	// if (isInSameClass(i, j, classGrid)) {
	// return new Point(i, j);
	// }
	// }
	// }
	// }
	// // search the complete grid
	// for (int i = 0; i < classGrid.length; i++) {
	// for (int j = 0; j < classGrid[0].length; j++) {
	// if (!(i == row && j == column) && isInSameClass(i, j, classGrid)) {
	// return new Point(i, j);
	// }
	// }
	// }
	// return null;
	// }
	// /**
	// * Check whether this point is a singleton, i.e. its class has only
	// itself.
	// *
	// * @param classGrid
	// * a 2-D array record the classes.
	// * @return a boolean value indicate whether this point is a singleton.
	// */
	// public boolean isSingleton(int[][] classGrid) {
	// // first check the points surrounding it
	// for (int i = row - 1; i <= row + 1; i++) {
	// for (int j = column - 1; j <= column + 1; j++) {
	// if (i >= 0 && i < classGrid.length && j >= 0 && j < classGrid[0].length
	// && !(i == row && j == column)
	// && isInSameClass(i, j, classGrid)) {
	// return false;
	// }
	// }
	// }
	// // check all element
	// for (int i = 0; i < classGrid.length; i++) {
	// for (int j = 0; j < classGrid[0].length; j++) {
	// if (!(i == row && j == column) && isInSameClass(i, j, classGrid)) {
	// return false;
	// }
	// }
	// }
	// return true;
	// }
	/**
	 * Get the class of this point.
	 * 
	 * @param classGrid
	 *            a 2-D array recording the classes of points.
	 * @return the class ID of this point
	 */
	public int getClassID(int[][] classGrid) {
		return getClassID(row, column, classGrid);
	}

	private static int getClassID(int rowIndex, int columnIndex,
			int[][] classGrid) {
		return classGrid[rowIndex][columnIndex];
	}

	/**
	 * Count how many points in a given set come from the same class as this
	 * point.
	 * 
	 * @param pts
	 *            a set of points.
	 * @param classGrid
	 *            a 2-D array recording the classes of points.
	 * @return the number of points in the given set that have the same class as
	 *         this point.
	 */
	public int countSameClass(Point[] pts, int[][] classGrid) {
		return countSameClass(getClassID(classGrid), pts, classGrid);
	}

	/**
	 * Count how many points in a given set come from the same class as the
	 * specified one.
	 * 
	 * @param classID
	 *            a class ID.
	 * @param pts
	 *            a array of points.
	 * @param classGrid
	 *            a 2-D array recording the classes of points.
	 * @return
	 */
	private static int countSameClass(int classID, Point[] pts,
			int[][] classGrid) {
		int count = 0;
		for (int i = 0; i < pts.length; i++) {
			if (pts[i].getClassID(classGrid) == classID) {
				count++;
			}
		}
		return count;
	}

	// /**
	// * Count the points in the complete set come from the same class as this
	// * point.
	// *
	// * @param classGrid
	// * a 2-D array recording the classes of points.
	// * @return the number of points in the complete set that have the same
	// class
	// * as this point.
	// */
	// public int countSameClass(int[][] classGrid) {
	// return countSameClass(getClassID(classGrid),classGrid);
	// }
	// /**
	// * Count the number of elements in a class.
	// * @param classID the ID of the class.
	// * @param classGrid a 2-D array recording the classes of points.
	// * @return the number of points in the given class.
	// */
	// public int countSameClass(int classID, int[][] classGrid){
	// int count = 0;
	// for (int i = 0; i < classGrid.length; i++) {
	// for (int j = 0; j < classGrid[0].length; j++) {
	// if (getClassID(i,j,classGrid)==classID) {
	// count++;
	// }
	// }
	// }
	// return count;
	// }
	/**
	 * Check whether two points are in the same class.
	 * 
	 * @param other
	 *            another point.
	 * @param classGrid
	 *            a 2-D array recording the classes of points.
	 * @return a boolean value indicate whether the two points are in the same
	 *         class.
	 */
	public boolean isInSameClass(Point other, int[][] classGrid) {
		if (this == other) {
			return true;
		}
		if (other == null) {
			return false;
		}
		return isInSameClass(other.row, other.column, classGrid);
	}

	/**
	 * Check whether the point with the given row and column index is in the
	 * same class as this point.
	 * 
	 * @param otherRow
	 *            the row of the other point.
	 * @param otherColumn
	 *            the column of the other point.
	 * @param aClassGrid
	 *            a 2-D array recording the classes of points.
	 * @return a boolean value indicate whether the two points are in the same
	 *         class.
	 */
	private boolean isInSameClass(int otherRow, int otherColumn,
			int[][] aClassGrid) {
		return aClassGrid[row][column] == aClassGrid[otherRow][otherColumn];
	}

	/**
	 * Get the neighbors of this points.
	 * 
	 * @param rowNumber
	 *            the number of rows in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @param ntype
	 *            neighbor type. See Enum type NeighborType.
	 * @return an Array of points containing neighbors of this point.
	 */
	public ArrayList<Point> getNeighbors(int rowNumber, int columnNumber,
			NeighborType ntype) {
		//initial an ArrayList with enough capacity
		ArrayList<Point> pts = new ArrayList<Point>(ntype.getNumber());
		int radius = ntype.getRadious();
		for (int i = row - radius; i <= row + radius; i++) {
			for (int j = column - radius; j <= column + radius; j++) {
				if (i >= 0 && i < rowNumber && j >= 0 && j < columnNumber) {
					Point pt = new Point(i, j);
					if (isNeighbor(pt, ntype)) {
						pts.add(pt);
					}
				}
			}
		}
		return pts;
	}
	/**
	 * Count the number of neighbors of this point.
	 * 
	 * @param rowNumber
	 *            the number of rows in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @param ntype
	 *            neighbor type. See Enum type NeighborType.
	 * @return the number of neighbors of this point.
	 * TODO add support for other kind of neighbors
	 */
	public int countNeighbors(int rowNumber, int columnNumber,
			NeighborType ntype) {
		switch(ntype){
		case FOUR://a faster way than loop
			return countFourNeighbors(rowNumber,columnNumber);
		case EIGHT://a faster way than loop
			return countEightNeighbors(rowNumber, columnNumber);
		default://use a loop to count the number of neighbors
			int number = 0;
			int radius = ntype.getRadious();
			Point pt = new Point();
			for(int i=row-radius; i<=row+radius; i++){
				for(int j=column-radius; j<=column+radius; j++){
					if(i>=0&&i<rowNumber&&j>=0&&j<columnNumber){
						pt.set(i, j);
						if(isNeighbor(pt,ntype)){
							number++;
						}
					}
				}
			}
			return number;
		}
	}
	/**
	 * Count the number of four-type neighbors.
	 * 
	 * @param rowNumber
	 *            the number of rows in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @return the number of eight-type neighbors.
	 */
	private int countFourNeighbors(int rowNumber, int columnNumber) {
		if (row == 0 || row == columnNumber - 1) {
			if (column == 0 || column == columnNumber - 1) {
				return 2;
			}
			return 3;
		}
		if (column == 0 || column == columnNumber - 1) {
			return 3;
		}
		return 4;
	}

	/**
	 * Count the number of four-type neighbors.
	 * 
	 * @param rowNumber
	 *            the number of row in the grid.
	 * @param columnNumber
	 *            the number of columns in the grid.
	 * @return the number of four-type neighbors.
	 */
	private int countEightNeighbors(int rowNumber, int columnNumber) {
		if (row == 0 || row == rowNumber - 1) {
			if (column == 0 || column == columnNumber - 1) {
				return 3;
			}
			return 5;
		}
		if (column == 0 || column == columnNumber - 1) {
			return 5;
		}
		return 8;
	}
	/**
	 * Set a new row index for this point.
	 * 
	 * @param aRow
	 *            a new row index.
	 */
	private void setRow(int aRow) {
		row = aRow;
	}

	/**
	 * Set a new column index for this point.
	 * 
	 * @param aColumn
	 *            a new column index.
	 */
	private void setColumn(int aColumn) {
		column = aColumn;
	}

	/**
	 * Set both row and column indexes for this point.
	 * 
	 * @param aRow
	 *            a new row index.
	 * @param aColumn
	 *            a new column index.
	 */
	public void set(int aRow, int aColumn) {
		setRow(aRow);
		setColumn(aColumn);
	}

	// /**
	// * Set row and column indexes as the same of another given point.
	// *
	// * @param other
	// */
	// public void set(Point other) {
	// set(other.row, other.column);
	// }

	/**
	 * Check whether two points are neighbors.
	 * 
	 * @param other
	 *            another point.
	 * @param ntype
	 *            NeighborType.
	 * @return a boolean value indicate whether the two points are neighbors of
	 *         the specified type.
	 */
	public boolean isNeighbor(Point other, NeighborType ntype) {
		int rowDiff = other.row - row;
		int columnDiff = other.column - column;
		int dist = rowDiff * rowDiff + columnDiff * columnDiff;
		//must exclude itself 
		return dist>0&&dist<=ntype.getDistance();
	}
}

package orientation2;

public class QuadraticPartitionPrior extends PartitionPrior {
	/**
	 *Quadratic coefficient, which controls the shape of the 
	 *quadratic adjusting function. 
	 *It doesn't matter too much as long as it's positive,
	 *because there's another weight coefficient for this adjusting function
	 */
	private double a;//used only be the adjusting function
	/**
	 * Minimized location of the quadratic adjusting function.
	 * You should give it your preferred value,
	 * i.e. a value you believe to be the number of partitions.
	 */
	private double b;//use only be the adjusting function
	
	//Since this adjusting function be varied by a constant, 
	//no need to given the constant coefficient, use 0 as default.
	public QuadraticPartitionPrior(double aA, double aB){
		a = aA;
		b = aB;
	}
	/**
	 * Implements the abstract adjusting method.
	 */
	public double adjusting(double x){
		//I choose a quadratic function for instance
		double diff = x-b;
		return a*diff*diff;
	}
	/**
	 * Implements the abstract clone method.
	 */
	@Override
	public PartitionPrior clone() {
		return new QuadraticPartitionPrior(a,b);
	}
	
}

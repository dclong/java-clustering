package orientation2;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;
import org.apache.commons.math.random.RandomGenerator;
import org.apache.commons.math.random.Well44497b;

public class Test {
	public static void main(String[] args) throws MathException {
		int[][] grid = new int[5][5];
		double[][] data = new double[5][5];
		RandomGenerator rg = new Well44497b(20111013);
		RandomDataImpl rng = new RandomDataImpl(rg);
		initializeGrid2(grid);
		initializeData(data, rng);
		printGrid(grid);
//		System.out.println(rng.nextInt(0, 4));
//		System.out.println(rng.nextInt(0, 4));
//		System.out.println(rng.nextInt(0, 4));
//		System.out.println(rng.nextInt(0, 4));
//		System.out.println(rng.nextInt(0, 4));
		// -----------------------------
		LLH llh = new NormalLLH();
		ParamPrior paramPrior = new NormalParamPrior(rng, 5, 5);
		PartitionPrior partPrior = new QuadraticPartitionPrior(1, 5);
		ClusterPartition clusterPart = new ClusterPartition.Builder(rng, llh,
				paramPrior, partPrior, data, grid).builder();
		clusterPart.runMCMC(100);
		printGrid(clusterPart.getClassGrid());
	}

	private static void printGrid(int[][] grid) {
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				System.out.print(grid[i][j]);
			}
			System.out.println("\n");
		}
	}

	private static void initializeData(double[][] data,
			RandomDataImpl rng) {
		// left top corner
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				data[i][j] = rng.nextGaussian(1, 1);
			}
		}
		// right top corner
		for (int i = 0; i < 2; i++) {
			for (int j = 3; j < 5; j++) {
				data[i][j] = rng.nextGaussian(10, 1);
			}
		}
		// left bottom corner
		for (int i = 3; i < 5; i++) {
			for (int j = 0; j < 3; j++) {
				data[i][j] = rng.nextGaussian(12, 1);
			}
		}
		// right bottom corner
		for (int i = 2; i < 5; i++) {
			for (int j = 3; j < 5; j++) {
				data[i][j] = rng.nextGaussian(3, 1);
			}
		}
	}
	
	private static void initializeGrid2(int[][] grid){
		for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
				grid[i][j] = i*grid[0].length + j;
			}
		}
	}
	
	private static void initializeGrid1(int[][] grid) {
		// left top corner
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				grid[i][j] = 0;
			}
		}
		// right top corner
		for (int i = 0; i < 2; i++) {
			for (int j = 3; j < 5; j++) {
				grid[i][j] = 3;
			}
		}
		// left bottom corner
		for (int i = 3; i < 5; i++) {
			for (int j = 0; j < 3; j++) {
				grid[i][j] = 15;
			}
		}
		// right bottom corner
		for (int i = 2; i < 5; i++) {
			for (int j = 3; j < 5; j++) {
				grid[i][j] = 13;
			}
		}
	}
}

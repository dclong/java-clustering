package orientation2;

import java.util.ArrayList;

public class TwoDArray {
	/**
	 * Get the 1-D index of an element in a regular 2-D array.
	 * @param rowIndex the row index of the element.
	 * @param columnIndex the column index of the element.
	 * @param columnNumber the number of columns of the regular 2-D array.
	 * @return the 1-D index of the element.
	 */
	public static int getUnivariateIndex(int rowIndex, int columnIndex, int columnNumber){
		return rowIndex*columnNumber+columnIndex;
	}
//	/**
//	 * Check whether two 2-D double are the same.
//	 * @param aArray
//	 * @param bArray
//	 * @return
//	 */
//	public static boolean equals(double[][] aArray, double[][] bArray){
//		if(aArray==bArray){
//			return true;
//		}
//		if (aArray.length != bArray.length||aArray[0].length!=bArray[0].length) {
//			return false;
//		}
//		for (int i = 0; i < aArray.length; i++) {
//			for (int j = 0; j < aArray[0].length; j++) {
//				if (aArray[i][j] != bArray[i][j]) {
//					return false;
//				}
//			}
//		}
//		return true;
//	}
	/**
	 * Check whether two 2-D integer arrays are the same.
	 * @param aArray
	 * @param bArray
	 * @return
	 */
	public static boolean equals(int[][] aArray, int[][] bArray){
		if(aArray==bArray){
			return true;
		}
		if (aArray.length != bArray.length||aArray[0].length!=bArray[0].length) {
			return false;
		}
		for (int i = 0; i < aArray.length; i++) {
			for (int j = 0; j < aArray[0].length; j++) {
				if (aArray[i][j] != bArray[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * Copy a 2-D double array to another 2-D double array.
	 * @param from the source 2-D double array.
	 * @param to the destination 2-D double array.
	 */
	public static void copyArray(double[][] from, double[][] to) {
		for (int i = 0; i < from.length; i++) {
			for (int j = 0; j < from[0].length; j++) {
				to[i][j] = from[i][j];
			}
		}
	}
	/**
	 * Copy a 2-D integer array to another 2-D integer array.
	 * @param from the source 2-D integer array.
	 * @param to the destination 2-D integer array.
	 */
	public static void copyArray(int[][] from, int[][] to){
		for(int i=0; i<from.length; i++){
			for(int j=0; j<from[0].length; j++){
				to[i][j] = from[i][j];
			}
		}
	}
	/**
	 * Split a 2-D integer array to different classes according to its values.
	 * @param aArray a 2-D integer array.
	 * @return an ArrayList of ArrayList of points.
	 */
	public static ArrayList<ArrayList<Point>> splitArray(int[][] aArray){
		//average dimension of the 2-D array, 
		//this is used as the initial capacity of the ArrayList
		//this might use more space, but can reduce the times of reallocation.
		int aveDim = (aArray.length + aArray[0].length)/2;
		//initialize an ArrayList of ArrayList<Integer> for storing indexes of different classes
		ArrayList<ArrayList<Point>> comList = new ArrayList<ArrayList<Point>>(aveDim);
		//initialize an ArrayList of Integer for storing current existing classes
		ArrayList<Integer> classIDs = new ArrayList<Integer>(aveDim);
		//split the 2-D array
		for(int i=0; i<aArray.length; i++){
			for(int j=0; j<aArray[0].length; j++){
				Point pt = new Point(i,j);
				//class ID of current point
				int cid = aArray[i][j];
				//find the index of the class id in classIDs
				int index = classIDs.indexOf(cid);
				if(index>=0){//an ArrayList<Point> for the class already exists
					comList.get(index).add(pt);
				}else{//an ArrayList<Point> for the class doesn't exist
					ArrayList<Point> aList = new ArrayList<Point>(aveDim);
					aList.add(pt);
					comList.add(aList);
					//push the current class id into the classIDs
					classIDs.add(cid);
				}
			}
		}
		return comList;
	}
}

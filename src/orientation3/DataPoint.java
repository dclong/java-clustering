package orientation3;

import java.util.ArrayList;

import dclong.util.Point;

public class DataPoint extends Point{
//	//the class this data point belongs to 
//	private int genre;
	//the observed data at this point
	//shall be generalized to rotation matrix later
	public final double data;
//	//Construct a data point with physical location and data information.
//	//The class of this data point is set to the default value -1 (which is invalid).
//	public DataPoint(Point aPoint, double aData){
//		this(aPoint,-1,aData);
//	}
	//Construct a data point with physical location, class and data information
	public DataPoint(Point aPoint, double aData){
		this(aPoint.getX(),aPoint.getY(),aData);
	}
	
//	public DataPoint(double aX,double aY, double aData){
//		this(aX,aY,aData);
//	}
	
	public DataPoint(double aX, double aY, double aData){
		super(aX,aY);
//		genre = aGenre;
		data = aData;
	}
	/**
	 * Define a distance between a point and a set of points, which is simply
	 * the sum of the distances between the point and points in the set.
	 * 
	 * @param aSet a set of points.
	 * @return the distance between this point and the set of point.
	 */
	public double distance(ArrayList<DataPoint> aSet) {
		int size = aSet.size();
		double sumDist = 0;
		for (int i = 0; i < size; i++) {
			sumDist += distance(aSet.get(i));
		}
		return sumDist;
	}
//	/**
//	 * Get the data at this point. 
//	 * @return the data at this point. 
//	 */
//	public double getData(){
//		return data;
//	}
//	@Override
//	public DataPoint clone(){
//		
//	}
//	/**
//	 * Get the class of this data point.
//	 * @return the class of this data point. 
//	 */
//	public int getGenre(){
//		return genre;
//	}
//	/**
//	 * Set a new class for this data point.
//	 * @param aGenre a new class. 
//	 */
//	public void setGenre(int aGenre){
//		genre = aGenre;
//	}
//	/**
//	 * Get the point with physical location information.
//	 * @return the point with physical location information.
//	 */
//	public Point getPoint(){
//		return point;
//	}
}

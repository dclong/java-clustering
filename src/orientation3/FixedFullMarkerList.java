package orientation3;

public class FixedFullMarkerList {
	private int[][] indices;
	private boolean[] fromSingleton;
	private int size;
	public FixedFullMarkerList(int capacity){
		indices = new int[capacity][4];
		fromSingleton = new boolean[capacity];
		size = 0;
	}
	
	public FixedFullMarkerList(int[][] aIndices, boolean[] aFromSingleton, int aSize){
		//copy array
		size = aSize;
		for(int i=0; i<size; i++){
			indices[i][0] = aIndices[i][0];
			indices[i][1] = aIndices[i][1];
			indices[i][2] = aIndices[i][2];
			indices[i][3] = aIndices[i][3];
			fromSingleton[i] = aFromSingleton[i];
		}
	}
	
	public FixedFullMarkerList clone(){
		return new FixedFullMarkerList(indices,fromSingleton,size);
	}
	
	public void clear(){
		size = 0;
	}
	
	public int size(){
		return size;
	}
	
	public void add(int firstIndex, int secondIndex, int thirdIndex, int fourthIndex, boolean fromSingleton){
		indices[size][0] = firstIndex;
		indices[size][1] = secondIndex;
		indices[size][2] = thirdIndex;
		indices[size][3] = fourthIndex;
		this.fromSingleton[size] = fromSingleton;
		size++;
	}
	
	public int getFirstIndex(int index){
		return indices[index][0];
	}
	
	public int getSecondIndex(int index){
		return indices[index][1];
	}
	
	public int getThirdIndex(int index){
		return indices[index][2];
	}
	
	public int getFourthIndex(int index){
		return indices[index][3];
	}
	
	public boolean getFromSingleton(int index){
		return fromSingleton[index];
	}
}

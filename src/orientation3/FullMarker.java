package orientation3;
/**
 * A class to record the location information (both before and after moving) of a point.
 * @author adu
 *
 */
public class FullMarker{
	//the index of the original group in the 
	//ArrayList of ArrayList of DataPoints (partition)
	public final int firstIndex;
	//the index of the selected point in the original group
	public final int secondIndex;
	//the index of the new group in the partition after the point is removed 
	//from its original place
	public final int thirdIndex;
	//the index in the new group where the selected point is moved to
	public final int fourthIndex;
	//indicate whether the original group is a singleton
	public final boolean fromSingleton;
	public FullMarker(int aFirstIndex, int aSecondIndex, int aThirdIndex, int aFourthIndex, boolean aFromSingleton){
		firstIndex = aFirstIndex;
		secondIndex = aSecondIndex;
		thirdIndex = aThirdIndex;
		fourthIndex = aFourthIndex;
		fromSingleton = aFromSingleton;
	}
}

package orientation3;

import java.util.ArrayList;

public abstract class LLH {
	/**
	 * An abstract clone method, which returns a new 
	 * object with identical states with this object.
	 */
	@Override
	public abstract LLH clone();
	/**
	 * An abstract llh method, which calculates 
	 * the log likelihood.
	 * @param x a real value.
	 * @param theta a real value.
	 * @return the log likelihood.
	 */
	public abstract double llh(double x, double theta);
	
	public double llh(DataPoint dp, double theta){
		return llh(dp.data,theta);
	}
	/**
	 * The logarithm of the joint likelihood,
	 * i.e. the sum of the log-likelihoods. 
	 * Note that this method works for an empty set. 
	 * @param aSet an ArrayList of points.
	 * @param data a 2-D array containing data.
	 * @param theta a real value.
	 * @return the logarithm of the joint likelihood.
	 */
	public double llhSum(ArrayList<DataPoint> aSet, double theta){
		int size = aSet.size();
		double sum = 0;
		DataPoint dp;
		for(int i=0; i<size; i++){
			dp = aSet.get(i);
			sum += llh(dp.data,theta);
		}
		return sum;
	}
}

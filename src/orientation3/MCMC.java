package orientation3;

import java.util.ArrayList;

import org.apache.commons.math.MathException;
import org.apache.commons.math.random.RandomDataImpl;

public class MCMC {
	RandomDataImpl rng;
	private Partition currentPartition;
	private ProposalPartition proposedPartition;
	
	private ArrayList<common.FreqPartition> posteriorDraws;
	
	private double jumpingRate;
	
	public MCMC(RandomDataImpl aRNG,Partition aPartition, ProposalPartition aProposalPartition,int initCapacity){
		rng = aRNG;
		currentPartition = aPartition.clone();
//		proposedPartition = proposedPartition.clone();
		posteriorDraws = new ArrayList<common.FreqPartition>(initCapacity);
	}
	
	public ArrayList<common.FreqPartition> getPosteriorDraws(){
		return posteriorDraws;
	}
	
	public void run(int step, int tuning){
		jumpingRate = 0;
		for(int i=0; i<step; i++){
			jump(tuning);
		}
		jumpingRate /= step;
	}
	
	public double getJumpingRate(){
		return jumpingRate;
	}
	
	public void burnIn(int step,int tuning){
		for(int i=0; i<step; i++){
			double lar = proposedPartition.propose(tuning);
			if(lar>0){
				currentPartition.forwardUpdate(proposedPartition);
				continue;
			}
			double acceptanceRatio = Math.exp(lar);
			try {
				if(rng.nextBinomial(1, acceptanceRatio)==1){
					currentPartition.forwardUpdate(proposedPartition);
					continue;
				}
				proposedPartition.backwardUpdate(currentPartition);
			} catch (MathException e) {
				e.printStackTrace();
				proposedPartition.backwardUpdate(currentPartition);
				System.out.println("Program recovered from excepton. You can ignore it.");
			}
			
		}
	}
	
	private void jump(int tuning){
		double lar = proposedPartition.propose(tuning);
		if(lar>0){
			acceptProposal();
			return;
		}
		double acceptanceRatio = Math.exp(lar);
		try {
			if(rng.nextBinomial(1, acceptanceRatio)==1){
				acceptProposal();
				return;
			}
			rejectProposal();
		} catch (MathException e1) {
			e1.printStackTrace();
			//this never happens in theory, if happens just ignore this step
			proposedPartition.backwardUpdate(currentPartition);
			System.out.println("Program recovered from exception. You can ignore it.\n");
		}
	}
	
	private void rejectProposal(){
		//push current partition into the draws list
		posteriorDraws.get(posteriorDraws.size()-1).increaseFrequency(1);
		//update partition
		proposedPartition.backwardUpdate(currentPartition);
	}
	
	private void acceptProposal(){
		//push the proposal partition into the draws list
		posteriorDraws.add(new common.FreqPartition(proposedPartition.getGenre(),1));
		//update partition
		currentPartition.forwardUpdate(proposedPartition);
		//update jumping rate
		jumpingRate++;
	}
}

package orientation3;

import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;

public abstract class ParamPrior {
	/**
	 * A globally shared random number generator.
	 * It will be used in the implementation of the two 
	 * abstract methods.
	 */
	protected RandomDataImpl rng;
	/**
	 * A constructor. 
	 * @param aRNG a globally shared random number generator.
	 */
	public ParamPrior(RandomDataImpl aRNG){
		rng = aRNG;
	}
	/**
	 * An abstract clone method, which returns
	 * a new object with identical states with this object.
	 */
	@Override
	public abstract ParamPrior clone();
	/**
	 * An abstract method next, which generate an
	 * observation from the distribution of the parameter prior.
	 * @return an observation from the distribution of the parameter prior.
	 */
	public abstract double next();
	/**
	 * An abstract method nextPost, which generate an
	 * observation from the posterior distribution of the 
	 * parameter prior.
	 * @param set an ArrayList of Points.
	 * @param data a 2-D array containing data.
	 * @return an random observation from the posterior
	 * distribution of the parameter prior.
	 */
	public abstract double nextPost(ArrayList<DataPoint> aSet);
}

package orientation3;

import java.util.ArrayList;

import dclong.util.Point;

public class Partition {
	protected ArrayList<ArrayList<DataPoint>> partition;
	/**
	 * Partial Partition Complexity. 
	 * See the definition in PartitionPrior.
	 */
	private double partialPartitionComplexity;
	
	public Partition(ArrayList<ArrayList<DataPoint>> partition){
		//make a shallow copy
		copyPartition(partition);
		partialPartitionComplexity = partialPartitionComplexity(partition);
	}
	
	public Partition(ArrayList<ArrayList<DataPoint>> partition, double aPartialPartitionComplexity){
		copyPartition(partition);
		partialPartitionComplexity = aPartialPartitionComplexity;
	}
	
	@Override
	public Partition clone(){
		return new Partition(partition,partialPartitionComplexity);
	}
	/**
	 * Make a shallow copy of the supplied partition. 
	 * The structure of the ArrayList is copied while the DataPoints are 
	 * reused. 
	 * @param aPartition
	 */
	private void copyPartition(ArrayList<ArrayList<DataPoint>> aPartition){
		int size = aPartition.size();
		partition = new ArrayList<ArrayList<DataPoint>>(size);
		for(int i=0; i<size; i++){
			ArrayList<DataPoint> source = aPartition.get(i);
			int step = source.size();
			ArrayList<DataPoint> destination = new ArrayList<DataPoint>(step);
			for(int j=0; j<step; j++){
				destination.add(source.get(j));
			}
			partition.add(destination);
		}
	}
	
//	public ArrayList<ArrayList<DataPoint>> getPartition(){
//		return partition;
//	}
	
	public double getPartialPartitionComplexity(){
		return partialPartitionComplexity;
	}
	
	public void decreasePartialPartitionComplexity(double by){
		partialPartitionComplexity -= by;
	}
	
	public void setPartialPartitionComplexity(double value){
		partialPartitionComplexity = value;
	}
	/**
	 * Calculate a partial complexity for a given partition.
	 * @param aPartition an ArrayList of ArrayList of points, which 
	 * represent a partition.
	 * @return the partial complexity of the partition.
	 */
	public static double partialPartitionComplexity(ArrayList<ArrayList<DataPoint>> aPartition){
		//get number of subsets
		int size = aPartition.size();
		if(size<=0){
			return 0;
		}
		double complexity = 0;
		for(int i=0; i<size; i++){
			complexity += setComplexity(aPartition.get(i));
		}
		return complexity/size;
	}
	
	/**
	 * Define a complexity for a set, which is the 
	 * average distances between points in the set.
	 * @param aSet an ArrayList of points.
	 * @return the distance between the point and the set.
	 */
	public static double setComplexity(ArrayList<DataPoint> aSet){
		int size = aSet.size();
		if(size<=1){
			return 0;
		}
		double sumDist = 0;
		for(int i=0; i<size-1; i++){
			for(int j=i+1; j<size; j++){
				sumDist += aSet.get(i).distance(aSet.get(j));
			}
		}
		return sumDist/(0.5*size*(size-1));
	}
//	public double getPartialPartitionComplexity(){
//		return partialPartitionComplexity;
//	}
	
//	public void setPartialPartitionComplexity(double aPartialPartitionComplexity){
//		partialPartitionComplexity = aPartialPartitionComplexity;
//	}
	
	/**
	 * Get class IDs for each data point. To make the representation unique, 
	 * the class ID of a group of points is the smallest ID of these points.
	 * @return an integer array containing class IDs for each data point.
	 */
	public int[] getGenre(){
		int[] genre = new int[Point.getCount()];
		int size = partition.size();
		for(int i=0; i<size; i++){
			ArrayList<DataPoint> currentList = partition.get(i);
			int step = currentList.size();
			int g = currentList.get(0).getID();
			for(int j=0; j<step; j++){
				genre[currentList.get(j).getID()] = g;
			}
		}
		return genre;
	}

	public void forwardUpdate(ProposalPartition aProposalPartition){
		moveForward(aProposalPartition.getProposalPath());
		partialPartitionComplexity = aProposalPartition.getPartialPartitionComplexity();
	}
	/**
	 * Move this partition forward along a path.
	 * @param path an ArrayList of FullMarkers.
	 */
	private void moveForward(FixedFullMarkerList path){
		int size = path.size();
		int firstIndex, secondIndex, thirdIndex, fourthIndex;
		for(int i=0; i<size; i++){
			firstIndex = path.getFirstIndex(i);
			secondIndex = path.getSecondIndex(i);
			thirdIndex = path.getThirdIndex(i);
			fourthIndex = path.getFourthIndex(i);
			//select a point
			ArrayList<DataPoint> dps = partition.get(firstIndex);
			DataPoint dp = dps.get(secondIndex);
			//remove it
			dps.remove(secondIndex);
			if(dps.isEmpty()){
				partition.remove(firstIndex);
			}
			if(thirdIndex>=partition.size()){
				//add the point as a singleton
				ArrayList<DataPoint> singleton = new ArrayList<DataPoint>();
				singleton.add(dp);
				partition.add(singleton);
			}else{
				partition.get(thirdIndex).add(fourthIndex,dp);
			}
		}
	}
}

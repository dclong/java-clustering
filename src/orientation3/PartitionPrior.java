package orientation3;

public abstract class PartitionPrior {
	/**
	 * An abstract clone method, which return as new
	 * object which has identical states with this object. 
	 */
	@Override
	public abstract PartitionPrior clone();
	/**
	 * An abstract adjusting function.
	 * @param x a real value.
	 * @return a real value.
	 */
	public abstract double adjusting(double x);
	/**
	 * Define a size penalty function for partitions.
	 * This penalty together with the partial complexity of partition 
	 * constitute the (complete) partition complexity (see the definition 
	 * for complete partition complexity. The concept of size penalty 
	 * here is adopted to make it easier to choose an adjusting function (see the
	 * definition for adjusting function), which can be some convex function
	 * with minimized at a user-preferred size. 
	 * You can overwrite this method if needed.
	 * @param size the size of a partition.
	 * @return the penalty at size.
	 */
	public double partitionSizePenalty(int size){
		return size/6d;
	}
	
	
	
//	/**
//	 * Define a (complete) complexity for a partition, 
//	 * which is the average of subsets' complexity (i.e. the partial 
//	 * partition complexity) plus n/6, where n is the number of subsets.
//	 * Note that the n/6 is introduced by myself to penalize a partition
//	 * of being having too many subsets. This penalty term is introduced 
//	 * by an approximation to make the partition complexity convex. It should
//	 * work for a nearly square grid. For other kind of grid, you probably
//	 * need a different penalty term.
//	 * @param aPart an ArrayList of ArrayList of points.
//	 * @return the complexity of the partition.
//	 */
//	private double partitionComplexity(ArrayList<ArrayList<Point>> aPart){
//		return PartitionPrior.partialPartitionComplexity(aPart) + partitionSizePenalty(aPart.size());
//	}
//	/**
//	 * Calculate the (complete) partition complexity for 
//	 * a given class grid (partition).
//	 * @param classGrid a 2-D array recording classes of points.
//	 * @return the (complete) partition complexity.
//	 */
//	public double partitionComplexity(int[][] classGrid){
//		return partitionComplexity(TwoDArray.splitArray(classGrid));
//	}
}

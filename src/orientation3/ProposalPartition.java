package orientation3;

import java.util.ArrayList;

import org.apache.commons.math.random.RandomDataImpl;

import dclong.util.Point;

public class ProposalPartition extends Partition {
	private RandomDataImpl rng;
	/**
	 * A PartitionPrior object. Initialization required.
	 */
	private PartitionPrior partPrior;
	/**
	 * A ParamPrior object. Initialization required.
	 */
	private ParamPrior paramPrior;
	/**
	 * A LLH object.
	 */
	private LLH llh;
	/**
	 * A list of FullMarkers with fixed capacity.
	 */
	private FixedFullMarkerList proposalPath;
	/**
	 * The weight coefficient for partition complexity part in the prior for
	 * partitions.
	 */
	private double complexityWeight;
	private double adjustingWeight;

	public ProposalPartition(RandomDataImpl aRNG,
			ArrayList<ArrayList<DataPoint>> partition,
			PartitionPrior aPartPrior, ParamPrior aParamPrior, LLH aLLH,
			FixedFullMarkerList aProposalPath, double aComplexityWeight, double aAdjustingWeight) {
		super(partition);
		rng = aRNG;
		partPrior = aPartPrior.clone();
		paramPrior = aParamPrior.clone();
		llh = aLLH.clone();
		proposalPath = aProposalPath.clone();
		complexityWeight = aComplexityWeight;
		adjustingWeight = aAdjustingWeight;
	}
	
	public ProposalPartition(RandomDataImpl aRNG,
			ArrayList<ArrayList<DataPoint>> partition, double partialPartitionComplexity,
			PartitionPrior aPartPrior, ParamPrior aParamPrior, LLH aLLH,
			FixedFullMarkerList aProposalPath, double aComplexityWeight, double aAdjustingWeight) {
		super(partition,partialPartitionComplexity);
		rng = aRNG;
		partPrior = aPartPrior.clone();
		paramPrior = aParamPrior.clone();
		llh = aLLH.clone();
		proposalPath = aProposalPath.clone();
		complexityWeight = aComplexityWeight;
		adjustingWeight = aAdjustingWeight;
	}

//	public ProposalPartition clone(){
//		return new ProposalPartition(rng,partition,getPartialPartitionComplexity(),partPrior,paramPrior,llh,proposalPath,complexityWeight,adjustingWeight);
//	}
	
	
	public FixedFullMarkerList getProposalPath(){
		return proposalPath;
	}
	// // make a mixture proposal
	// public void propose(double lambda) {
	//
	// }
	/**
	 * Propose a multi-step-ahead partition.
	 * 
	 * @param step
	 *            the number of step ahead.
	 * @return the log acceptance ratio of the multi-step-ahead proposal
	 *         partition with respect to the original partition.
	 */
	public double propose(int step) {
		proposalPath.clear();
		// the log acceptance ratio
		double lar = 0;
		for (int i = 0; i < step; i++) {
			lar += propose();
		}
		return lar;
	}

	/**
	 * Propose a 1-step-ahead partition.
	 * 
	 * @return a the log acceptance ratio of the new partition w.r.t to the
	 *         original one, i.e. the 1-step back partition.
	 */
	private double propose() {
		// randomly select a point
		// the index^th point in the partition will be moved
		int index = rng.nextInt(1, Point.getCount());
		// find the class index and within-class index of the point
		ShortMarker shortMarker = getOldMarker(index);
		// remove the point from the partition
		int firstIndex = shortMarker.firstIndex;
		ArrayList<DataPoint> dps = partition.get(firstIndex);
		int secondIndex = shortMarker.secondIndex;
		DataPoint dp = dps.get(secondIndex);
		// dps.remove(secondIndex);
		int thirdIndex;
		int selectedSize = dps.size();
		if (selectedSize == 1) {
			// //remove dps
			// proposedPartition.remove(firstIndex);
			// randomly select a class to accept the point
			thirdIndex = rng.nextInt(0, partition.size() - 2);
			if (thirdIndex >= firstIndex) {
				thirdIndex++;
			}
			// //move the point to the end of the list
			// proposedPartition.get(thirdIndex).add(dp);
		} else {
			// randomly select a class to accept the point
			// int classNumber = proposedPartition.size();
			thirdIndex = rng.nextInt(0, partition.size() - 1);
			if (thirdIndex >= firstIndex) {
				thirdIndex++;
			}
			// //move the point to the right place
			// if(thirdIndex>=classNumber){
			// //move the point to a singleton
			// ArrayList<DataPoint> singleton = new ArrayList<DataPoint>();
			// singleton.add(dp);
			// proposedPartition.add(singleton);
			// }else{
			// proposedPartition.get(thirdIndex).add(dp);
			// }
		}
		// --------- calculate log acceptance ratio ---------------
		double lppDiff = logPartitionPriorDiff(dps, dp, selectedSize,
				thirdIndex);
		int fourthIndex = getFourthIndex(dp, thirdIndex);
		double llhDiff = logLHDiff(dps, firstIndex, dp, secondIndex,
				thirdIndex, fourthIndex);
		if (selectedSize == 1) {
			// must modify the third index
			if (thirdIndex > firstIndex) {
				thirdIndex--;
			}
			proposalPath.add(firstIndex, secondIndex, thirdIndex, fourthIndex,
					true);
		} else {
			proposalPath.add(firstIndex, secondIndex, thirdIndex, fourthIndex,
					false);
		}
		return lppDiff + llhDiff;
	}

	/**
	 * Get the within-new-class index of the point to be moved.
	 * 
	 * @param dp
	 *            the point to be moved.
	 * @param thirdIndex
	 *            the index of the new class where the point is to be move.
	 * @return the within-new-class index of the point to be moved.
	 */
	private int getFourthIndex(DataPoint dp, int thirdIndex) {
		// move to a singleton, thus the fourthIndex is 0
		if (thirdIndex >= partition.size()) {
			return 0;
		}
		// check boundaries
		ArrayList<DataPoint> dps = partition.get(thirdIndex);
		int aimID = dp.getID();
		int lowIndex = 0;
		int diff = aimID - dps.get(lowIndex).getID();
		if (diff < 0) {
			return lowIndex;
		}
		int highIndex = lowIndex + diff;
		int size = dps.size();
		if (highIndex >= size) {
			highIndex = size - 1;
			diff = aimID - dps.get(highIndex).getID();
			if (diff > 0) {
				return size;
			}
			lowIndex = highIndex + diff;
			if (lowIndex < 0) {
				lowIndex = 0;
			}
		}
		// use binary search to find the right place
		int middleIndex = 0;
		while (highIndex > lowIndex) {
			middleIndex = (highIndex + lowIndex) / 2;
			diff = aimID - dps.get(middleIndex).getID();
			if (diff > 0) {
				lowIndex = middleIndex + 1;
				highIndex = Math.min(highIndex, middleIndex + diff);
			} else {
				highIndex = middleIndex;
				lowIndex = Math.max(lowIndex, middleIndex + diff);
			}
		}
		return highIndex;
	}

	/**
	 * The difference between the logarithms of the likelihoods for the proposed
	 * partition and the current one.
	 * 
	 * @return the difference between the logarithm of the likelihoods.
	 */
	private double logLHDiff(ArrayList<DataPoint> selectedClass,
			int firstIndex, DataPoint selectedPoint, int secondIndex,
			int thirdIndex, int fourthIndex) {
		int selectedSize = selectedClass.size();
		int moveToSize;
		if (thirdIndex >= partition.size()) {
			moveToSize = 0;
		} else {
			moveToSize = partition.get(thirdIndex).size();
		}
		if (moveToSize == 0) {// moved to a singleton, 3 parameters needed
			// generate parameter for the original set S1
			double theta1 = paramPrior.nextPost(selectedClass);
			// generate parameter for the singleton set P0
			double theta3 = paramPrior.next();
			// generate parameter for the left set S1/P0
			double theta4 = paramPrior.next();
			// calculate the log likelihood
			double s1 = llh.llhSum(selectedClass, theta1);
			selectedClass.remove(secondIndex);
			ArrayList<DataPoint> singleton = new ArrayList<DataPoint>();
			singleton.add(selectedPoint);
			partition.add(singleton);
			double s3 = llh.llhSum(selectedClass, theta3);
			double s4 = llh.llh(selectedPoint, theta4);
			return s3 + s4 - s1;
		}
		ArrayList<DataPoint> moveToClass = partition.get(thirdIndex);
		if (selectedSize == 1) {
			// singleton moved to another set, 3 parameters needed
			// generate parameter for the singleton
			double theta1 = paramPrior.nextPost(selectedClass);
			double theta2 = paramPrior.nextPost(moveToClass);
			// generate parameter for the resulting set
			double theta4 = paramPrior.next();
			double s1 = llh.llhSum(selectedClass, theta1);
			double s2 = llh.llhSum(moveToClass, theta2);
			moveToClass.add(fourthIndex, selectedPoint);
			selectedClass.clear();
			partition.remove(firstIndex);
			double s4 = llh.llhSum(moveToClass, theta4);
			return s4 - s1 - s2;
		}
		// moved out to another class, 4 parameters needed
		double theta1 = paramPrior.nextPost(selectedClass);
		double theta2 = paramPrior.nextPost(moveToClass);
		double theta3 = paramPrior.next();
		double theta4 = paramPrior.next();
		double s1 = llh.llhSum(selectedClass, theta1);
		double s2 = llh.llhSum(moveToClass, theta2);
		selectedClass.remove(secondIndex);
		double s3 = llh.llhSum(selectedClass, theta3);
		moveToClass.add(fourthIndex, selectedPoint);
		double s4 = llh.llhSum(moveToClass, theta4);
		return s3 + s4 - s1 - s2;
	}

	private ShortMarker getOldMarker(int index) {
		// must initialize in the following way
		int firstIndex = -1;
		int secondIndex = 0;
		while (index > 0) {
			firstIndex++;
			secondIndex = index - 1;
			index -= partition.get(firstIndex).size();
		}
		ShortMarker.SINGLETON.firstIndex = firstIndex;
		ShortMarker.SINGLETON.secondIndex = secondIndex;
		return ShortMarker.SINGLETON;
	}

	private double logPartitionPriorDiff(ArrayList<DataPoint> selectedClass,
			DataPoint selectedPoint, int selectedSize, int thirdIndex) {
		int classNumber = partition.size();
		int moveToSize;
		if (thirdIndex >= classNumber) {
			moveToSize = 0;
		} else {
			moveToSize = partition.get(thirdIndex).size();
		}
		if (moveToSize == 0) {// move to a singleton
			if (selectedSize == 2) {
				// there are 2 points in the selected class
				double setDiff = Partition
						.setComplexity(selectedClass);
				double ppComDiff = (setDiff + getPartialPartitionComplexity())
						/ (classNumber + 1);
				double comDiff = ppComDiff
						+ partPrior.partitionSizePenalty(classNumber)
						- partPrior.partitionSizePenalty(classNumber + 1);
				double adjDiff = partPrior.adjusting(classNumber)
						- partPrior.adjusting(classNumber + 1);
				decreasePartialPartitionComplexity(ppComDiff);
				return complexityWeight * comDiff + adjustingWeight * adjDiff;
			}// there are multiple points in the class of the selected point
			double setDiff = (selectedPoint.distance(selectedClass)
					/ (selectedSize - 1) - Partition
						.setComplexity(selectedClass))
					/ (selectedSize - 2)
					* 2d;
			double ppComDiff = (setDiff + getPartialPartitionComplexity())
					/ (classNumber + 1);
			double comDiff = ppComDiff
					+ partPrior.partitionSizePenalty(classNumber)
					- partPrior.partitionSizePenalty(classNumber + 1);
			double adjDiff = partPrior.adjusting(classNumber)
					- partPrior.adjusting(classNumber + 1);
			decreasePartialPartitionComplexity(ppComDiff);
			return complexityWeight * comDiff + adjustingWeight * adjDiff;
		}// move to an existing set
		ArrayList<DataPoint> moveToClass = partition.get(thirdIndex);
		if (selectedSize == 1) {// the selected point is a singleton
			double setDiff = (selectedPoint.distance(moveToClass) / moveToSize - Partition
					.setComplexity(moveToClass)) / (moveToSize + 1) * 2d;
			double ppComDiff = -(setDiff + getPartialPartitionComplexity())
					/ (classNumber - 1);
			double comDiff = ppComDiff
					+ partPrior.partitionSizePenalty(classNumber)
					- partPrior.partitionSizePenalty(classNumber - 1);
			double adjDiff = partPrior.adjusting(classNumber)
					- partPrior.adjusting(classNumber - 1);
			decreasePartialPartitionComplexity(ppComDiff);
			return complexityWeight * comDiff + adjustingWeight * adjDiff;
		}// the selected point is not a singleton
			// set difference for selected class and its variation
		double setDiff1 = Partition.setComplexity(selectedClass);
		if (selectedSize > 2) {
			setDiff1 = (selectedPoint.distance(selectedClass)
					/ (selectedSize - 1) - Partition
						.setComplexity(selectedClass))
					/ (selectedSize - 2)
					* 2d;
		}
		double setDiff2 = (Partition.setComplexity(moveToClass) - selectedPoint
				.distance(moveToClass) / moveToSize)
				/ (moveToSize + 1) * 2d;
		double ppComDiff = (setDiff1 + setDiff2) / classNumber;
		double comDiff = ppComDiff; // no penalty or adjusting change
		decreasePartialPartitionComplexity(ppComDiff);
		return comDiff;
	}
	
	public void backwardUpdate(Partition aPartition){
		moveBackward(proposalPath);
		setPartialPartitionComplexity(aPartition.getPartialPartitionComplexity());
	}
	/**
	 * Move this partition backward along a path.
	 * @param path an ArrayList of FullMarkers.
	 */
	private void moveBackward(FixedFullMarkerList path){
		int size = path.size();
		int firstIndex, secondIndex, thirdIndex, fourthIndex;
		boolean fromSingleton;
		for(int i=size-1; i>=0; i--){
			firstIndex = path.getFirstIndex(i);
			secondIndex = path.getSecondIndex(i);
			thirdIndex = path.getThirdIndex(i);
			fourthIndex = path.getFourthIndex(i);
			fromSingleton = path.getFromSingleton(i);
			ArrayList<DataPoint> dps = partition.get(thirdIndex);
			DataPoint dp = dps.get(fourthIndex);
			dps.remove(fourthIndex);
			if(dps.isEmpty()){
				partition.remove(thirdIndex);
			}
			if(fromSingleton){
				//the partition results from moving a singleton
				ArrayList<DataPoint> singleton = new ArrayList<DataPoint>();
				singleton.add(dp);
				partition.add(firstIndex,singleton);
			}else{
				partition.get(firstIndex).add(secondIndex,dp);
			}
		}
	}
}

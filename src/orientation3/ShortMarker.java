package orientation3;
/**
 * A class to record the original location of a point to be moved. 
 * @author adu
 *
 */
public class ShortMarker {
	public static final ShortMarker SINGLETON = new ShortMarker();
	//the index of the old class of the point in the partition
	public int firstIndex;
	//the index of the point in the old class
	public int secondIndex;
	private ShortMarker(){
	}
}
